=====================
cc-infinite
=====================

**Computing Competences.
Innovative learning approach for non-IT students.**

.. figure:: img/ccinflogo.png
  :scale: 50

CC Infinite è rivolto a chiunque sia interessato all'apprendimento della programmazione. Non vogliamo limitare questa competenza solo agli studenti di informatica o agli appassionati di informatica. Crediamo che ogni persona, non necessariamente legata al settore IT, possa rendere più piacevole il proprio lavoro al computer e velocizzare il proprio lavoro conoscendo le regole della programmazione. L'obiettivo principale del progetto sarà aumentare l'innovazione e l'interdisciplinarietà dell'istruzione superiore sviluppando un corso di programmazione per studenti non informatici e valutandone l'efficacia in 4 università europee.

Questo manuale è stato creato come risultato del `Progetto CC Infinite <https://ccinfinite.smcebi.edu.pl>`_ cofinanziato nell'ambito del programma dell'Unione Europea `Erasmus+ <https://www.erasmusplus.eu/>`_.

###################################
Corso interattivo di programmazione
###################################

.. toctree::
   :maxdepth: 1

   main/it_main_01_intro.rst
   main/it_main_02_variables.rst
   main/it_main_03_strings.rst
   main/it_main_04_io.rst
   main/it_main_05_collections.rst
   main/it_main_06_conditions.rst
   main/it_main_07_while_loop.rst
   main/it_main_08_for_loop.rst
   main/it_main_09_functions.rst
   main/it_main_10_functions.rst
   main/it_main_11_module.rst
   main/it_main_12_oop.rst

I materiali didattici che offriamo sono stati sviluppati nell'ambito di un consorzio internazionale.

.. figure:: img/uni.png
    :scale: 80

.. figure:: img/ccdisclaimer.png
    :scale: 80
