.. _oop:

**************************************
Programmazione orientata agli oggetti
**************************************

.. index:: class, instance, object

Siamo giunti all'ultimo capitolo di questo manuale. Parleremo di oggetti. Nel caso del linguaggio Python, tutto è un oggetto, quindi questo dovrebbe essere probabilmente il primo capitolo, ma è troppo tardi per quello.
Non discuteremo qui in modo approfondito oggetti, classi e le loro istanze. Ti diremo solo come creare rapidamente una **classe** e la sua **istanza**, che rappresenta questa classe.

     Tutto è un oggetto perché può essere assegnato a una variabile o essere un argomento di una funzione.

Per costruire un oggetto che rappresenti alcuni dati e consenta la manipolazione (modifica, estrazione di informazioni rilevanti...), è necessario programmare la classe. In Python, utilizziamo la seguente costruzione.

.. code-block:: Python

  class ClassName:
    BODY_CLASS

Le classi possono rappresentare qualsiasi oggetto inventato da noi. Possiamo costruire classi che descrivono un'anatra, una pera o un tavolino da caffè. Questi veri e propri "oggetti" hanno attributi fisici specifici, come colore e peso, ma possiamo anche pensare alle caratteristiche che distinguono gli oggetti. Per l'anatra e la pera, potrebbe essere la loro specie. Per un tavolo, potrebbe essere il tipo di materiale.

La classe definisce lo stato e il comportamento degli oggetti. Possiamo usarlo per descrivere quali proprietà ha un dato oggetto e cosa possiamo fare con esso.
Scriviamo una classe ``Pera`` per illustrare l'idea. Possiamo descrivere una tale pera parlando della sua varietà, colore (che è probabilmente correlato alla varietà), peso, dimensione (relativo al peso), gusto...

.. code-block:: Python

    class Pear:
        "'
        The Pear oggetto descrive le proprietà delle pere.
        "'

        def __init __ (self, variety, weight, taste):
            self.variety = variety
            self.weight = weight
            self.taste = taste

Questo è tutto! Abbiamo creato una classe che descrive le pere. Non c'è ancora niente di interessante che possiamo fare con questa classe. Abbiamo sviluppato una classe che definisce un **oggetto**. Ora possiamo crearne uno.
Potresti notare che proprio sotto la definizione di ``class Pear:`` mettiamo la stringa ``'''L'oggetto Pear descrive le proprietà delle pere'''``. Come per le funzioni, è una stringa che rappresenta la documentazione della classe (docstring). Scrivere documentazione è sempre un'ottima idea.
Creiamo un oggetto basato su questa classe.

.. activecode:: en_main_1201
    :caption: Pear

    class Pear:
        '''
        The Pear oggetto descrive le proprietà delle pere.
        '''

        def __init__(self, variety, weight, taste):
            self.variety = variety
            self.weight = weight
            self.taste = taste

    pear = Pear('General Leclerc', 130, 'sour and sweet')
    print(pear.variety)

Ora la variabile ``pear`` memorizza un oggetto della classe ``Pear``.
Se tratto la classe come un pattern che deve essere riempito, allora l'oggetto ``pear`` sarà solo una copia di tale pattern della classe "Pear", ma *con valori specifici (argomenti)* assegnati a i parametri formali della classe. In questo caso:

    ``variety`` <- 'General Leclerc'
    ``weight`` <- 130
    ``taste`` <- 'sour and sweet'

.. index:: instance, instance attribute, class attribute

La chiameremo **istanza di classe** o semplicemente **istanza**. Questa istanza ha i suoi **attributi**: ``variation``, ``weight`` e ``flavor``, che in questo caso sono identici agli **attributi della classe**. Possiamo riferirci a loro come a variabili ordinarie (perché sono anche variabili) usando * notazione punto *

.. code-block:: Python

    >>> pear.variety
    'General Leclerc'

Creando una nuova istanza della classe con una chiamata

.. code-block:: Python

    pear = Pear('General Leclerc', 130, 'sour and sweet')

inseriamo i parametri che abbiamo dichiarato nella definizione della funzione ``__init __(self, variant, weight, taste)``, una funzione eccezionale, come vedrai tra un minuto.

.. index:: init, __init__

Funzione ``__init__``
======================

Questa eccezionale *funzione speciale* ``__init__``viene usata per passare argomenti alle nuove istanze della classe ``Pear``. Per usare argomenti ovunque nella classe (oggetto) dobbiamo assegnarli a **attributi**. Possiamo suddividere gli attributi in attributi di classe e di istanza. Quelli che appartengono alle istanze iniziano nella classe con il prefisso ``self``.

.. index:: self

La variabile ``self`` è una rappresentazione di istanza. Ogni funzione nel corpo della classe dovrebbe iniziare con ``self`` [#self]_. Nel nostro esempio,

.. code-block:: Python

    pear = Pear('General Leclerc', 130, 'sour and sweet')

all'interno del corpo della classe, ``self`` si trasforma magicamente in ``pera``. Infatti, ``self`` non deve essere chiamato ``self``. Può essere chiamato qualsiasi cosa. Qualunque cosa venga chiamata (sebbene raccomandiamo ``self``), sarà una **variabile che rappresenta un'istanza all'interno del corpo della classe**. Per fare riferimento a qualsiasi attributo di istanza, dobbiamo precedere questo attributo nella classe con la variabile ``self`` e utilizzare il punto di riferimento.

.. index:: get, set, getter, setter

Getter e setter
====================

Non dovremmo fare riferimento direttamente agli attributi dell'istanza (``varietà, peso, sapore``), indipendentemente dal fatto che vogliamo leggere il valore presente o assegnarne uno nuovo. Ci sono funzioni speciali chiamate **getters** (per leggere, i loro nomi iniziano con la parola ``get_``) e **setter** (per cambiare il valore, ``set_``). Sono costruite esattamente come le normali funzioni (con ``def``), e il loro primo argomento deve essere ``self``.

.. activecode:: en_main_1202
    :caption: Pear - getter e setter

    class Pear:
        '''The class Pear describes the properties of pears.'''

        def __init__(self, variety, weight, taste):
            self.variety = variety
            self.weight = weight
            self.taste = taste

        def get_weight(self):
            return self.weight

        def get_variety(self):
            return self.variety

        def get_taste(self):
            return self.taste

        def set_weight(self, x):
            self.weight = x

        def set_variety(self, x):
            self.variety = x

        def set_taste(self, x):
            self.taste = x


    # esempio
    pear = Pear('General Leclerc', 330, 'sour and sweet')

    # riferimento al metodo get_taste() dell'istanza pear
    print("What is the flavor of a pear?")
    print("Pear is", pear.get_taste())

    # impostando la variabile del sapore con set_taste(TASTE) per l'istanza della pera
    pear.set_taste('extremely sweet')
    print("What is the flavor of a pear?")
    print("Pear is", pear.get_taste())

Come puoi vedere, le funzioni ``get_taste`` o ``set_taste`` sono chiamate come funzioni normali, ma c'è un nome di istanza davanti ad essa (qui: ``pear``). Si noti che sebbene nel corpo della classe ``Pear`` la funzione ``get_taste`` (riga 15) abbia un parametro formale ``self``, quando si chiama questa funzione (righe 42 o 48) **lo facciamo non fornire un argomento**. Questa è una notazione (interfaccia) tipica per chiamare funzioni che appartengono a un'istanza. Chiamiamo queste funzioni **metodi**.

    **Metodo** è una funzionalità relativa all'istanza. Con i metodi possiamo influenzare lo stato degli oggetti (istanze).

In particolare, prende un argomento in meno della sua controparte (funzione) nel corpo della classe perché ``self`` riceve l'indirizzo dell'istanza (``pear``) da cui chiamiamo il metodo. Gli argomenti rimanenti vengono visualizzati nella chiamata come in una funzione tradizionale.
Di nuovo - magicamente il nome dell'istanza viene sostituito nella classe sotto la variabile ``self``.

.. index:: str, __str__

La classe ``Square``
=====================

Ora è il momento per un esempio di classe più significativo. Costruiremo una classe relativamente semplice per rappresentare i quadrati.
Lo chiameremo ``Quadrato``. Che sorpresa! Denominare le classi con lettere maiuscole nella convenzione *CamelCase* è una buona pratica consigliata dagli sviluppatori Python. La classe avrà funzionalità minime: calcolerà l'area e il perimetro dei quadrati.

.. activecode:: en_main_1203
    :caption: Class Square
    :enabledownload:

    class Square:
        "" "Class Square" ""

        def __init__(self, a):
            self.set_side(a)

        def get_side(self):
            return self.side

        def set_side(self, var):
            if isinstance(var, (int, float)) and var > 0:
                self.side = var
            else:
                print('Side must be a number > 0')

        def area(self):
            return self.get_side() ** 2

        def perimeter(self):
            return 4 * self.get_side()

        def __str__(self):
            return "{} of area {} and perimeter {}".format(
                self.__class__.__name__, self.area(), self.perimeter())


    k1 = Square(10)
    print(k1)
    k2 = Square(3.1415)
    print(k2)

Sperimenta con la classe sopra. Costruisci altri quadrati.
Quando hai finito, leggi il paragrafo seguente. Dovrebbe chiarire cosa e come funziona una classe del genere. Infine, alcuni esercizi ti aspettano.

Per descrivere un quadrato, abbiamo solo bisogno di un parametro: la lunghezza del lato. Conoscendo questo parametro, possiamo calcolare qualsiasi quantità che descriva un quadrato, come la sua area o perimetro. Il parametro formale ``a`` fornito durante la creazione dell'istanza. Questa volta, per passare il valore del parametro ``a`` della funzione ``__init__`` all'attributo di istanza denominato ``side`` (visibile nel programma sopra come ``self.side``), dobbiamo usa la funzione di supporto ``self.set_side(a)``. Questa funzione è scritta in modo difensivo e non ti consentirà di specificare valori diversi dai numeri positivi del tipo ``int`` o ``float``. Tuttavia, se l'utente decide di inserire qualche altro valore, vedrà l'informazione ``Lato deve essere un numero > 0`` e la variabile ``lato`` non verrà creata. Con l'input corretto, come nei due esempi di istanza, la variabile ``side`` assumerà il valore fornito dall'utente, quindi potremo calcolare l'area e il perimetro del quadrato.

Entrambe le funzioni di utilità ``area`` e ``perimeter`` accettano solo un parametro formale ``self``, cioè l'istanza corrispondente. Grazie a ciò, l'insieme completo di attributi e metodi associati all'istanza sarà disponibile per queste funzioni attraverso la costruzione ``self.ATTRIBUTE``. Tra le altre cose, abbiamo il metodo ``get_side`` che restituisce la lunghezza del lato di un quadrato. Dobbiamo fare riferimento ad esso per poter calcolare l'area e il perimetro. Potremmo usare direttamente la chiamata ``self.side``. Sarà solo un modo diverso per calcolare le stesse caratteristiche senza usare l'idea di getter e setter. Entrambi gli approcci hanno i loro pro e contro. Le funzioni stesse sono semplici. Quando calcoliamo l'area, aumentiamo la lunghezza del lato alla seconda potenza e, quando calcoliamo il perimetro, sommiamo le lunghezze di tutti e quattro i lati uguali.

Il prossimo nuovo concetto è la funzione speciale ``__str__``. Ricordi ancora: ref:`problema di concatenazione di stringhe <string_concatenation>`? Per convertire qualsiasi oggetto in una stringa, dovevamo usare la funzione di proiezione ``str``. Questa funzione cerca negli oggetti della funzione speciale ``__str__`` e la chiama. Quindi, quando costruiamo una tale funzione, creiamo una rappresentazione del nostro oggetto nel tipo ``str``. Inoltre - la funzione ``print`` mostra anche ciò che programmiamo nella funzione ``__str__``. Quando si crea una classe, vale la pena dedicare un po' di tempo alla programmazione di questa funzione speciale. Grazie a ciò, possiamo descrivere chiaramente l'istanza dell'oggetto con cui lavoreremo in seguito.

.. topic:: Esercizio

     Aggiungi una funzione che calcola la lunghezza della diagonale alla classe ``Square``. Usa la formula :math:`d = a \sqrt{2}`.

.. topic:: Esercizio

     Sulla base della classe ``Square``, costruisci la classe ``Rettangolo``. Ricorda che in generale hanno due lati, ``a`` e ``b`` di lunghezze diverse.


.. rubric:: Note a piè di pagina

.. [#self] mentiamo un po' qui - non tutte le funzioni devono iniziare con ``self``, ma per ora lasciamo le cose così.
