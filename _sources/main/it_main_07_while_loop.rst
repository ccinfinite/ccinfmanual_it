.. _while_loop:

***************
Ciclo ``while``
***************

A volte dobbiamo eseguire una sequenza di istruzioni più e più volte, forse anche all'infinito. Nei linguaggi di programmazione, in questi casi utilizziamo i loop. Python ha due di queste affermazioni che funzionano su principi leggermente diversi.

.. index:: while, loop

Il costrutto Python universale che consente di eseguire più volte un blocco di istruzioni è l'istruzione while. Come la ramificazione, si basa su una condizione logica.

.. code-block:: python

    while CONDITION:
        WHILE_BLOCK

Finché la condizione è soddisfatta (uguale a ``True``), verrà eseguito il blocco di istruzioni ``WHILE_BLOCK``. Come puoi vedere, il blocco dell'istruzione ricomincia dopo i due punti. È tempo per un semplice esempio di un'operazione in loop. Per restituire i numeri da 1 a 5 elevati alla seconda potenza, possiamo utilizzare il concetto di loop.

.. activecode:: en_main_0701
    :caption: quadrati di numeri da 1 a 5

    number = 1
    while number <= 5:
        square_number = number ** 2
        print ('{}^2 = {}'. format (number, square_number))
        number = number + 1

L'ultima riga (numero 5) è essenziale. Se lo dimentichiamo, cosa che accade spesso inaspettatamente, il ciclo continuerà per sempre.
Di seguito troverai una tabella che presenta passo passo tutti i valori della variabile ``numero`` e del quadrato e il risultato del test ``numero >= 5``. Nota che la variabile ``square_number`` non verrà calcolata per ``number = 6``.

+------+--------------+-----------+
|number|square_number |number <= 5|
+======+==============+===========+
|  1   |      1       |    True   |
+------+--------------+-----------+
|  2   |      4       |    True   |
+------+--------------+-----------+
|  3   |      9       |    True   |
+------+--------------+-----------+
|  4   |     16       |    True   |
+------+--------------+-----------+
|  5   |     25       |    True   |
+------+--------------+-----------+
|  6   |              |   False   |
+------+--------------+-----------+

Possiamo risolvere qualsiasi compito di programmazione con due costrutti condizionali, ``if`` e ``while``. Non sarà sempre facile, a volte può sembrare impossibile, ma di solito possiamo farlo. Ora cercheremo di trovare numeri divisibili per 2 e 3, ma quelli maggiori di 33 e minori di 67.

.. activecode:: en_main_0702
    :caption: numeri divisibili per A e B nel limite dall'inizio alla fine

    start, stop = 33, 67
    A, B = 2, 3

    num = start
    while num <= stop:
        if (num % A == 0) and (num % B == 0):
            print('{} is divisible by {} and {}'.format(num, A, B))
        num = num + 1

Come puoi vedere sopra, abbiamo messo entrambe le condizioni tra parentesi nell'istruzione condizionale. In questo modo, possiamo *raggruppare* calcoli o istruzioni che dovrebbero essere eseguiti separatamente e non siamo sicuri se una notazione senza parentesi funzionerà. In questo caso, la rimozione delle parentesi non cambierà nulla, in quanto il prodotto logico ``and`` ha una delle `priorità più basse tra gli operatori <https://docs.python.org/3/reference/expressions.html# precedenza-operatore>`_. Indipendentemente dalla precedenza dell'operazione, le parentesi spesso aumentano notevolmente la leggibilità del codice e ti invitiamo a usarle (con saggezza!).

.. topic:: Esercizio

     Modificare il programma sopra per calcolare la **somma** di tutti i numeri divisibili per 2 e 3, maggiori di 33 e minori di 67.

.. index:: while - else

Per quanto riguarda i rami, possiamo usare l'istruzione ``else`` nel ciclo ``while``.

.. code-block:: python

    while CONDITION:
        WHILE_BLOCK
    else:
        ELSE_BLOCK

Possiamo capirlo esattamente come la costruzione ``se-else``. Quando 'CONDITION' non viene soddisfatta, verrà eseguito il blocco di istruzioni in 'else'. Abbiamo due di queste opzioni:

* il ciclo ``while`` verrà eseguito fino alla fine e il blocco ``else`` verrà chiamato dopo di esso

    .. code-block:: python

        i = 1
        while i < 8:
            print(2 ** i)
            i += 1
        else:
            print('end of loop')

* il ciclo ``while`` non verrà eseguito affatto (la condizione non sarà soddisfatta), il blocco ``else`` verrà eseguito

    .. code-block:: python

        i = 10
        while i < 8:
            print(2 ** i)
           i += 1
        else:
            print('while loop will not execute at all')


.. index:: break, continue

Le frasi ``break`` e ``continue``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se prevediamo che il ciclo si interrompa ad un certo punto, possiamo usare la parola chiave ``break``.

.. activecode:: en_main_0703
    :caption: break

    j = 10
    while True:
        if j > 23:
            print('stop: j = {} is greater than 23'.format(j))
            break
        j += 3

.. note:: **while True**

     Tale costruzione viene utilizzata quando vogliamo costruire un ciclo infinito. Poiché la condizione ``True`` sarà sempre vera, dobbiamo usare un altro modo per fermare il ciclo. Possiamo usare l'istruzione ``break`` come nell'esempio sopra. Possiamo anche chiamare :ref:`subroutine <subroutines>`, che controllerà il programma. Parleremo presto di subroutine.


L'affermazione ``continua`` ha esattamente il comportamento opposto. Quando si incontra, le istruzioni nel blocco del ciclo smetteranno di essere eseguite e il ciclo ruoterà di nuovo, indipendentemente dallo stato del programma.

.. activecode:: en_main_0704
    :caption: continue (il numero -3 non verrà stampato)

    j = -5
    while j <= 5:
        j += 1
        if j == -3:
            print('we omit {}'.format(j))
            continue
        print('cube {} to {}'.format(j, j**3))


.. topic:: Esercizio

     A volte, quando eseguiamo un programma per computer, ci vengono poste varie domande, solitamente necessarie per configurarlo correttamente. A volte la risposta si limita a decidere "sì" o "no", quindi se scriviamo "ok", il programma non saprà cosa intendiamo - dovremo inserire con precisione o la parola "sì" o la parola "no" , e se ne diamo un'altra, il programma tornerà alla stessa domanda e continuerà a chiedere finché non inseriamo una di queste parole. Usando un ciclo infinito e l'istruzione ``break``, scrivi un programma del genere. Di seguito vi presentiamo lo scheletro di questo programma. Al posto dei commenti, inserisci il tuo codice.

.. activecode:: en_main_0705
    :caption: exercise
    :enabledownload:

    while True:
        ans = input('Inserisci sì o no: ')
        # se sì: stampa 'Accetto!' e termina il ciclo (programma)
        # in caso negativo: stampa 'Nessun consenso!' e termina il ciclo (programma)
        # se diverso: chiedi nuovamente di inserire sì o no
        break
