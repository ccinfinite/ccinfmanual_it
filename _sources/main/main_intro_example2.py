from string import ascii_lowercase
import requests

url = 'https://www.gutenberg.org/cache/epub/3070/pg3070.txt'
txt = requests.get(url).text

fixed_txt = ""
for letter in txt.replace('\n', ' '):
    l = letter.lower()
    if l in ascii_lowercase + ' ':
        fixed_txt += l

zipf = {}
for word in fixed_txt.split():
    if word in zipf:
        zipf[word] += 1
    else:
        zipf[word] = 1

for z in sorted(zipf.items(), key=lambda x: x[1], reverse=True)[:10]:
    print('{0[0]}\t{0[1]}'.format(z))
