.. _complex_data_types:

*****************************************
Alcuni dei tipi complessi più popolari
*****************************************

A volte è conveniente raggruppare gli oggetti insieme, ad esempio, creando un elenco dei nomi degli studenti in classe, un elenco delle canzoni preferite dei Pearl Jam o creando un catalogo di libri. Possiamo anche mantenere i voti per articoli specifici in collezioni simili. Costruire variabili separate per le tue cinque canzoni preferite dei Pearl Jam è fattibile ma non molto efficiente.

.. code-block:: python

   pj_no1 = 'Black'
   pj_no2 = 'Crazy Mary'
   pj_no3 = 'Rearviewmirror'
   pj_no4 = 'Better Man'
   pj_no5 = 'Sirens'

Sarebbe più saggio fare una lista di queste canzoni e mettere la tua canzone preferita *nell'elenco attuale* nella prima posizione, la tua seconda canzone preferita nella seconda posizione, ecc...

.. index:: list, elenco, oggetto modificabile

Elenchi
~~~~~~~

Un **elenco** (tipo ``list`` di Python) può tornare utile.
L'elenco è il tipo più generale di sequenza. Proprio come gli oggetti :ref:`strings <strings>` di questo tipo sono sequenze ordinate. Il principale vantaggio delle liste è la semplicità di elaborazione dei loro elementi. Le operazioni sono simili a quelle sulle stringhe in quanto possiamo tagliarle, fare riferimento a indici o verificarne la lunghezza. Possiamo creare una variabile di tipo ``list`` in diversi modi. Il più semplice sarà usare le parentesi quadre, che è un'**interfaccia** dedicata alle liste.

.. code-block:: python

    L1 = [10, 20, 30]
    L2 = []

Il primo oggetto chiamato ``L1`` sarà una lista con tre elementi numerici ``10, 20, 30`` e il secondo ``L2`` sarà una lista vuota (una lista senza elementi). Pertanto, la prima lunghezza è tre e la seconda è 0: puoi verificarla con la stessa funzione che hai usato per controllare la dimensione della stringa ``len(list)``. Tornando all'esempio della tracklist, sembrerebbe così.

.. activecode:: en_main_0501
    :caption: Pearl Jam's top five

    pj = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print(pj)


Come puoi vedere, la prima canzone dell'elenco è la canzone che era stata precedentemente assegnata alla variabile che denota la hit numero uno. Per fare riferimento a questo nome, useremo lo stesso riferimento per indice delle stringhe.

.. code-block:: python

    pj[0]

Restituirà 'Nero'.

.. index:: concatenating lists

Gli elenchi possono essere concatenati usando l'operatore ``+``.
Possiamo creare una nuova lista combinando queste due liste con l'operatore di addizione ``L1 + L2``. Il nuovo elenco sarà composto da tutti gli elementi di entrambi gli elenchi. Gli elementi ``L1`` saranno all'inizio e gli elementi ``L2`` si troveranno alla fine.

.. activecode:: en_main_0502
    :caption: Pearl Jam's Top Ten

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    pj2 = ['Alive', 'Yellow Ledbetter', 'Jeremy', 'Even Flow', 'Once']
    pj = pj1 + pj2
    print(pj)


Possiamo modificare le liste. La modifica più semplice consiste nel cambiare un elemento specifico usando il riferimento ``list[index]`` e usare l'operatore di assegnazione. Possiamo, ad esempio, decidere che a partire da oggi, la quinta miglior canzone dei Pearl Jam per noi non sarà *Sirens*, ma *Hail, Hail*. In questo caso è sufficiente fare riferimento al quinto posto della lista (ricordando che iniziamo a numerare da zero) e assegnare un nuovo brano.

.. showeval:: showEval_list_mod
   :trace_mode: true

   pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
   ~~~~
   {{'Sirens' --> 'Hail, Hail'}}{{pj1[4] = 'Hail, Hail'}}
   {{pj1[4] = 'Hail, Hail'}}{{}}
   pj1 --> ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', {{'Sirens'}}{{'Hail, Hail'}}]

Provate voi stessi. Puoi anche cambiare *Hail, Hail* con un titolo completamente diverso.

.. activecode:: pl_main_0503
    :caption: modifica in riferimento a un indice

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print('Oryginalna lista', pj1, sep='\n')
    pj1[4] = 'Hail, Hail'
    print('Zmodyfikowana lista', pj1, sep='\n')


Come per le stringhe, possiamo usare :ref:`sequenza di taglio <sequence_slicing>`, tramite riferimento noto.

.. code-block:: python

    list[start_index:end_index:step]


Modifiche e altre operazioni specifiche agli elenchi
-----------------------------------------------------

.. index:: OOP, object, method, attribute

Python è un linguaggio orientato agli oggetti. Ve ne parleremo meglio alla fine di questo manuale. Per ora è sufficiente sapere che un oggetto contiene sia dati che metodi per manipolare questi dati. Puoi interpretare i dati solo come alcuni valori che assegni alle variabili. Ad esempio, ``L = [1, 2, 3]`` sarebbe un elenco chiamato ``L``con tre elementi ``1, 2, 3``.

Per operare su tale lista (così come su altri oggetti) utilizzando metodi in essa incorporati, utilizziamo il cosiddetto **riferimento con un punto**. La sintassi per tale riferimento è.

.. code-block:: python

    # riferimento al metodo
    object.method(arguments)
    # riferimento a un attributo (campo)
    object.attribute

.. index:: append

Gli elenchi hanno diversi metodi di questo tipo e li discuteremo alla fine di questa sezione. Per ora, ci concentreremo su uno dei più utilizzati: il metodo ``append`` per estendere l'elenco con un altro elemento. Se abbiamo già la lista sopra menzionata delle cinque canzoni preferite dei Pearl Jam ``pj1``, ma vogliamo aggiungere altri due pezzi ad essa, allora possiamo scrivere

.. activecode:: en_main_0504
    :caption: modifica da un riferimento di indice

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print('Original list', pj1, sep='\n')
    pj1.append('Hail, Hail')
    print('Extended list', pj1, sep='\n')
    pj1.append('You Are')
    print('List of 7 elements', pj1, sep='\n')

Speriamo che tu lo capisca perché ora è il momento di un esercizio ...

.. topic:: Esercizio

     Usando il metodo ``append``, estendi la playlist originale ``pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']`` con altri 5 brani: 'Alive' , 'Yellow Ledbetter', 'Jeremy', 'Even Flow', 'Once' (o qualsiasi altra cosa tu scelga). Puoi usare il *ActiveCode* sopra o programmare la risposta nel taccuino di Jupyter.



Esistono molti metodi per manipolare gli elenchi.

* ``L.append(obj)``: aggiunge un elemento alla fine dell'elenco ``L``
* ``L.extend(new_list)``: Estende l'elenco includendo tutti gli elementi dell'elenco specificato ``new_list``
* ``L.insert(idx, obj)``: inserisce l'elemento ``obj`` nella posizione data nell'elenco ``idx``
* ``L.remove(obj)``: rimuove dalla lista il primo elemento incontrato ``obj``; se non è presente tale voce nell'elenco, viene segnalato un errore
* ``L.pop(idx)``: rimuove un elemento dall'elemento ``idx`` specificato nell'elenco e lo restituisce come risultato; se non viene fornito alcun indice, ``L.pop()`` rimuove e restituisce l'ultimo elemento nell'elenco
* ``L.index(obj)``: Restituisce l'indice del primo elemento nell'elenco
* ``L.count(obj)``: Restituisce il numero di occorrenze di ``obj`` nell'elenco
* ``L.sort()``: ordina gli elementi nella lista stessa; questa è un'operazione che modifica irreversibilmente la lista ``L``
* ``L.reverse()``: inverte l'ordine degli elementi nell'elenco stesso

Proveremo alcuni di questi: Faremo una lista L e aggiungeremo i quadrati dei numeri da 1 a 10. Quindi rimuoveremo e aggiungeremo alcuni numeri con vari metodi.

.. activecode:: en_main_0505
    :caption: alcune operazioni sulle liste

    L = [10, 20, 30]
    print('start: L =', L)

    L.insert(0, 1001)
    L.append(100)
    print('append + insert: L =', L)

    L.pop(2)
    del L[2]
    print('pop + del: L =', L)

    L.reverse()
    print('reverse: L =', L)


.. index:: tuple

Tuple
~~~~~~

Le tuple sono liste che non possiamo modificare. Li creiamo
con parentesi ``()``.

.. code-block:: python

    three_elements_tuple = (1, 2, 3)
    single_element_tuple = ('Ala',)
    empty_tuple = ()

Nell'esempio sopra, ``tre_elementi_tupla`` è una tupla con tre elementi (``len(tupla a tre elementi)`` restituirà il numero 3),
Una ``single_element_tuple`` è una tupla con un elemento
e ``tupla_vuota`` è una tupla vuota.
Nota la virgola obbligatoria quando crei tuple a elemento singolo ``('Ala',)``. Le tuple possono essere concatenate usando l'operatore ``+``.

.. activecode:: en_main_0506
    :caption: tuple concatenation

    k1 = (1, 2, 3)
    k2 = ('Ala', 'has', 'a cat')
    k3 = k1 + k2
    print(k3)

Le tuple come oggetti immutabili hanno i metodi limitati a ``count`` e ``index``, che funzionano esattamente come i metodi di elenco appropriati.

.. activecode:: en_main_0507
    :caption: two tuple methods

    k1 = (1, 1, 3, 2, 3, 3)
    print('k1.count(3):', k1.count(3))
    print('k1.index(3):', k1.index(3))

Un elemento tupla non può essere rimosso con il comando ``del``, poiché non possiamo modificarlo.


.. index:: dict

Dizionari
~~~~~~~~~~~~

Oltre ai due **tipi sequenziali** di cui sopra, un altro tipo complesso utile e frequentemente utilizzato è **dizionari**. Ciò che li distingue da elenchi, tuple e stringhe è la loro mancanza di ordine e il fatto che ogni elemento in un dizionario è composto da due parti: una chiave e un valore. La chiave è una quantità che identifica il valore nel dizionario. Costruiamo dizionari usando parentesi graffe.

.. code-block:: python

  dictionary = {key1: value1, key2: value2, key3: value3}


Se, ad esempio, vorremmo creare un database di informazioni sui compleanni dei nostri amici, possiamo utilizzare un dizionario. Useremo le chiavi per segnare i nomi degli amici e il valore sarà la data di nascita.


.. code-block:: python

     birthday = {'Amanda': 'January 12', 'Barbara': 'February 17', 'Carol': 'August 2'}


Quando vogliamo ricordare la data di nascita di Amanda, dobbiamo fare riferimento all'elemento il cui ``key = 'Amanda'``, usando parentesi quadre, simili a liste, tuple e stringhe.


.. activecode:: en_main_0508
    :caption: Birthday dictionary

    birthday = {'Amanda': 'January 12', 'Barbara': 'February 17', 'Carol': 'August 2'}
    print(birthday['Amanda'])


In questo modo, possiamo scoprire in quale giorno i nostri amici compiono gli anni se lo dimentichiamo accidentalmente.

.. topic:: Esercizio

      Prova il precedente *ActiveCode* sostituendo il nome con un altro nome dall'elenco. Puoi anche aggiungere le tue coppie ``nome: data di nascita``.

In un modo relativamente semplice, possiamo modificare elementi già esistenti del dizionario. Basta fare riferimento a un elemento del dizionario con una chiave e assegnare un nuovo valore.

.. activecode:: en_main_0509
    :caption: Modifica di un elemento del dizionario

    birthday = {'Amanda': 'January 12', 'Barbara': 'February 17', 'Carol': 'August 2'}
    print(birthday)
    birthday['Amanda'] = 'August 30'
    print(birthday)

Simile ai tipi complessi discussi in precedenza, i dizionari hanno molti metodi integrati che consentono di manipolarli. Sono tutti disponibili utilizzando il punto di riferimento. Tra poco ne tratteremo alcuni. Ora ci concentreremo sulla possibilità di ampliare il dizionario con nuovi elementi. L'opzione più semplice è inserire una nuova chiave e un nuovo valore come se si volesse modificare una coppia esistente.

.. activecode:: en_main_0510
    :caption: Aggiungi un elemento del dizionario

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    birthday['Mike'] = 'November 20'
    print(birthday)

.. topic:: Esercizio

     Nel *ActiveCode* sopra, aggiungi il nome e il giorno di nascita di qualcuno che conosci. Potrebbe anche essere qualcuno famoso come Albert Einstein.

.. index:: del

Per rimuovere facilmente una coppia ``chiave: valore`` dal dizionario, possiamo usare il comando ``del`` per rimuovere le variabili dallo spazio dei nomi.

.. activecode:: en_main_0511
    :caption: Elimina un elemento del dizionario

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    del birthday['Carol']
    print(birthday)

Oltre a queste operazioni di base che utilizzano l'interfaccia delle parentesi quadre, i dizionari possono essere modificati utilizzando i metodi incorporati in essi. Di seguito troverai un elenco di metodi con una breve panoramica. Assumiamo che ``d`` sia un dizionario.


* ``d.clear()`` Rimuove tutti gli elementi dal dizionario ``d``
* ``d.copy()`` Restituisce una copia del dizionario ``d``
* ``d.fromkeys(klucze, default_value)`` Restituisce il dizionario ``d`` con le chiavi specificate ``keys`` e il valore impostato sulla variabile facoltativa ``default_value``
* ``d.get(klucz, default_value)`` Restituisce il valore della chiave data ``key``, se non c'è chiave restituisce il valore della variabile ``default_value``
* ``d.items()`` Restituisce una lista contenente una tupla per ogni coppia ``key: value``
* ``d.keys()`` Restituisce un elenco contenente le chiavi di un dizionario
* ``d.pop(klucz)`` Cancella un elemento con la chiave specificata ``key``
* ``d.popitem()`` Elimina l'ultima coppia chiave-valore inserita
* ``d.setdefault(key, value)`` Restituisce il valore della ``key`` specificata. Se la ``chiave`` non esiste, crea una coppia ``chiave: valore`` nel dizionario ``d``
* ``d.update(d)`` Aggiorna il dizionario ``s`` con elementi del dizionario ``d`` sovrascrivendo le coppie esistenti
* ``d.values()`` Restituisce un elenco di tutti i valori nel dizionario

Le operazioni apprese consentono una manipolazione relativamente libera dei dizionari. Ad esempio, possiamo utilizzare due modi per modificare il dizionario della data di nascita.

.. activecode:: en_main_0512
    :caption: Modifica di un elemento del dizionario

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    print(birthday)
    # 1
    birthday['Carol'] = 'August 3'
    print(birthday)
    # 2
    birthday.update({'Carol': 'August 4'})
    print(birthday)
