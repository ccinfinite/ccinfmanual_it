**********
Prefazione
**********

Nel 1977, Ken Olsen, fondatore e direttore di lunga data di Digital Equipment Corporation, dichiarò: *"Non c'è motivo per cui qualcuno vorrebbe un computer in casa."*. È interessante notare che lui stesso era il proprietario di un computer del genere. Se parliamo di computer, era ovviamente la preistoria. Oggi nessuno si interroga sull'usabilità dei computer. A volte aiutano a guarire le persone, a volte servono semplicemente a tenersi in contatto con gli amici. I personal computer si trovano in oltre il 48% delle famiglie in tutto il mondo. Viene utilizzato in media dal 15% delle persone nel mondo. Poco più del 35% di noi utilizza smartphone, anche un tipo di computer. Se aggiungiamo al fatto che utilizziamo Internet in media per oltre 6 ore al giorno, si scopre che la profezia del signor Olsen è stata una delle opinioni più mancate della storia. In totale, rimaniamo in due terre irreali per più della metà della nostra vita poiché dormiamo in media quasi 7 ore al giorno.

La maggior parte di noi usa computer, smartphone o tablet. Tutti utilizziamo il browser per navigare in Internet o applicazioni dedicate per visualizzare o modificare le foto. Utilizziamo programmi per scrivere lettere, saggi, e-mail e per contattare tramite i social media. Alcuni di noi lavorano utilizzando applicazioni più specializzate, altri hanno solo bisogno di un browser web e di un client di posta elettronica.

A volte incontriamo una situazione in cui il programma che stiamo attualmente utilizzando ci consente di eseguire tutte le attività necessarie, a volte ci mancano alcune funzionalità e dobbiamo risolvere il compito in modo diverso. A volte puoi trovare un programma diverso che lo farà, a volte no. A volte l'implementazione di attività pianificate richiede molto tempo, ma può essere eseguita utilizzando i programmi per computer familiari. Poi ci vuole solo tempo e pazienza (e preferibilmente qualcuno che controlli se tutto è a posto).

Imparare il linguaggio di programmazione non significa padroneggiare uno dei linguaggi di programmazione. Ce ne sono migliaia - dai più `popolari <https://www.tiobe.com/tiobe-index>` _ come Java, C, Python, C ++, C#, PHP, a quelli estremamente esoterici come già storico INTERCAL o minimalista Brainf*ck.

Questo è ovviamente un passaggio necessario, ma non è l'obiettivo principale.
L'abilità più importante che acquisisci è la capacità di capire il cuore del problema che stiamo affrontando e di suddividerlo correttamente nelle sue prime parti, così come sapremo risolvere facilmente e poi mettere insieme queste soluzioni in modo da ottenere la risposta alla domanda originaria. Ecco come funziona un tipico programmatore. Questo metodo ha anche il suo nome: divide et impera ed è uno dei principali paradigmi di programmazione e algoritmi.

L'apprendimento di questo modello può essere il più grande valore aggiunto di questo corso. Puoi provare a trovare una via d'uscita da diverse situazioni della vita semplicemente risolvendo parti più piccole del grande problema che devi affrontare. A volte vale anche la pena programmare passo dopo passo il tuo giorno successivo.

.. note::
    Puoi provare subito i seguenti programmi su un notebook Jupyter (IPython).
    Scarica questo file, caricalo sul tuo server Jupyter o aprilo
    al Colaboratorio Google.

    file: `notebook <https://nbviewer.org/urls/bitbucket.org/ccinfinite/ccinfmanual_it/raw/22205a3c3bef62677575b9d8d5308da3b2fe5366/_sources/main/it_main_01_intro_code.ipynb>`_.

Come organizzare le fotografie?
===============================

Tempo per qualche esempio, qualche applicazione più specifica di tutta questa idea di programmazione. Molte persone hanno un archivio di foto digitali sui propri computer o su dischi rigidi esterni. Spesso conserviamo queste foto nei cataloghi, chiamiamo questi cataloghi con un luogo che abbiamo visitato o con l'evento a cui abbiamo partecipato. A volte aggiungiamo una data al nome e talvolta abbiamo cataloghi principali associati alla data, ad es.
::

    -2019
      ᒻ--Trip to Krakow
      ᒻ--Fireworks show
      ᒻ--John birthday party

    -2018
      ᒻ--Holidays in Bari
      ᒻ--Weekend in Paris

Le immagini che si trovano all'interno, di solito hanno il nome dato dalla fotocamera, che di solito assomiglia a questo
::

  IMG20180821232.JPG  IMG20180821233.JPG  IMG20180821234.JPG  IMG20180821235.JPG
  IMG20180821236.JPG  IMG20180821237.JPG  IMG20180821238.JPG  IMG20180821239.JPG
  IMG20180821240.JPG  IMG20180821241.JPG  IMG20180821242.JPG  IMG20180821243.JPG
  IMG20180821244.JPG  IMG20180821245.JPG  IMG20180821246.JPG  IMG20180821247.JPG

Forse, solo per passione per l'ordine, vorremmo che le foto nei cataloghi avessero anche nomi significativi che identifichino immediatamente il contenuto della foto, ad esempio `2018_Holidays_in_Bari_001.jpg`. Il metodo più semplice e oneroso sarà semplicemente rinominare manualmente tutti questi file. Un altro potrebbe essere l'uso di uno dei programmi grafici in grado di apportare tale modifica nella cosiddetta modalità batch. Puoi anche scrivere un semplice programma Python...

.. literalinclude:: main_intro_example1.py
   :linenos:
   :caption: :download:`the script <main_intro_example1.py>`.

L'unica cosa che dovresti sapere è che invece di "2019" dobbiamo inserire il nome del catalogo, o anche l'anno a cui siamo interessati, e nel luogo "festa di compleanno di John" inseriamo il nome della directory in cui ci memorizzare le foto i cui nomi vogliamo solo cambiare. Naturalmente, dobbiamo anche sapere come eseguire un programma del genere, ma non dobbiamo preoccuparci del cambio di nome stesso o del fatto che faremo un errore durante la numerazione delle foto. Una volta che avremo padroneggiato la capacità di eseguire programmi Python che abbiamo scritto noi stessi, useremo gli stessi comandi ogni volta.

Sembra un po' complicato, ma impareremo tutto presto. L'importante è che abbiamo bisogno solo di 12 righe di codice (anche se in realtà ce ne sono 10) affinché il computer faccia tutto il lavoro per noi. Inoltre, questo programma può essere modificato semplicemente in modo che cerchi nella cartella principale in cui conserviamo le foto e cambi automaticamente i nomi di tutti i file in modo simile, dando all'inizio il nome della foto un anno , quindi il nome dell'evento e infine la propria numerazione.


OK, tempo per un problema più accademico ...

Quante volte compare una parola in un testo scritto?
======================================================

Sicuramente tutti coloro che leggono libri una volta si sono chiesti quale parola ricorre
più spesso. Oppure c'è qualche regola che descrive la relazione
con la frequenza della parola e il rango della parola...
Ok, probabilmente nessuno sta davvero pensando a queste cose,
piuttosto tutti si concentrano sul contenuto del libro ;)
Tuttavia, questo è un problema piuttosto interessante (almeno per noi).

L'idea è di contare quante volte si trova una determinata parola
testo considerato. Se consideriamo un testo semplice, ad es. la canzone "Hello, Goodbye" dei Beatles, quindi possiamo semplicemente contare quante volte una determinata parola appare in essa.

    You say, „Yes”, I say, „No”

    You say, „Stop” but I say, „Go, go, go”

    Oh no

    You say, „Goodbye”, and I say, „Hello, hello, hello”

    I don’t know why you say, „Goodbye”, I say, „Hello, hello, hello”

    I don’t know why you say, „Goodbye”, I say, „Hello”

Non prendiamo in considerazione il caso delle lettere, sono sempre le stesse parole. Consigliamo a tutti di contare il numero di volte in cui appare una determinata parola (ce ne sono 15) e poi di sistemare le parole dalla più comune alla più rara. Dovresti ottenere una tabella simile

========== ==========
say        10
i          7
hello      7
you        5
go         3
goodbye    3
no         2
don’t      2
know       2
why        2
yes        1
stop       1
but        1
oh         1
and        1
========== ==========

È corretto? Se il testo è semplice, il compito non è difficile. Proviamo con un testo leggermente più complicato.

    Having conceived the idea he proceeded to carry it out with
    considerable finesse. An ordinary schemer would have been content
    to work with a savage hound. The use of artificial means to make
    the creature diabolical was a flash of genius upon his part. The
    dog he bought in London from Ross and Mangles, the dealers in
    Fulham Road. It was the strongest and most savage in their
    possession. He brought it down by the North Devon line and walked
    a great distance over the moor so as to get it home without
    exciting any remarks. He had already on his insect hunts learned
    to penetrate the Grimpen Mire, and so had found a safe
    hiding-place for the creature. Here he kennelled it and waited
    his chance.

    But it was some time coming. The old gentleman could not be
    decoyed outside of his grounds at night. Several times Stapleton
    lurked about with his hound, but without avail. It was during
    these fruitless quests that he, or rather his ally, was seen by
    peasants, and that the legend of the demon dog received a new
    confirmation. He had hoped that his wife might lure Sir Charles
    to his ruin, but here she proved unexpectedly independent. She
    would not endeavour to entangle the old gentleman in a
    sentimental attachment which might deliver him over to his enemy.
    Threats and even, I am sorry to say, blows refused to move her.
    She would have nothing to do with it, and for a time Stapleton
    was at a deadlock.

Non così semplice. Prima di tutto - abbiamo a che fare con molte più parole. In secondo luogo, di solito non si ripetono affatto. Non presenteremo l'intera tabella qui, poiché ci sono fino a 154 righe, ma le parole più comuni assomigliano a questa

========== ==========
the        14
to         11
his        9
it         8
a          8
and        8
he         7
was        6
with       4
of         4
in         4
would      3
========== ==========

Il computer è lo strumento perfetto per eseguire le stesse attività più e più volte.
Gli informatici dicono che per fare la stessa cosa molte volte, devi dire al computer di usare *loop*.
Hai già visto un esempio del suo utilizzo nel problema precedente.

Nel programma seguente, l'idea di utilizzare un ciclo si presenta tre volte. Nella prima, controlliamo lettera per lettera se il carattere del testo originale appare nell'alfabeto inglese. La seconda volta contiamo le parole una per una. Il terzo ciclo `for` viene utilizzato per mostrare i risultati, dalla parola più comune a quella più rara.

Il programma stesso è interattivo, puoi avviarlo facendo clic sul pulsante "Esegui", puoi anche modificarlo, interromperlo e ripararlo.

.. activecode:: Zipf_law
   :coach:
   :caption: Un programma interattivo che conta il numero di volte in cui una determinata parola compare nel testo.

   txt = """
   You say, „Yes”, I say, „No”
   You say, „Stop” but I say, „Go, go, go”
   Oh no
   You say, „Goodbye”, and I say, „Hello, hello, hello”
   I don’t know why you say, „Goodbye”, I say, „Hello, hello, hello”
   I don’t know why you say, „Goodbye”, I say, „Hello”
   """

   fixed_txt = ""
   for letter in txt.replace("\n", " "):
       l = letter.lower()
       if l in "abcdefghijklmnopqrstuvwxyz ’":
           fixed_txt += l

   zipf = {}
   for word in fixed_txt.split():
       if word in zipf:
           zipf[word] += 1
       else:
           zipf[word] = 1

   for z in sorted(zipf.items(), key=lambda x: x[1], reverse=True):
       print("{0[0]}\t{0[1]}".format(z))

Dai un'occhiata alle righe 11, 17 e 23. Puoi vedere le parole chiave ``for``. Significa che abbiamo appena programmato tre loop. Loop è un meccanismo che consente al computer di eseguire alcune attività più e più volte.

Ora prova a sostituire il testo con un altro.


Bene, ma come funzionerebbe per l'intero libro? Il suddetto estratto dalla prosa viene dal romanzo poliziesco di Sir Arthur Conan Doyle, intitolato Il mastino dei Baskerville. Nella traduzione che troverai sulla pagina web del Progetto Gutenberg `gutenberg.org <https://www.gutenberg.org/ebooks/2852>` _ l'intero testo ha oltre 62.000 parole e ci pone un compito piuttosto impossibile. Cioè, puoi ovviamente contare le parole date usando un metodo sistematico, ma questo tipo di attività sequenziali "trova e aggiungi" dovrebbe essere eseguita dalla macchina.

Allora come gestisci l'intero libro?

.. literalinclude:: main_intro_example2.py
   :linenos:
   :emphasize-lines: 4,10,20
   :caption: Link to :download:`this script <main_intro_example2.py>`.


Copia questo codice sul notebook Jupyter oppure scarica ed esegui il programma nella console. Dovrebbe calcolare la cardinalità e mostrarti le prime 10 parole più comuni nel libro.

Ora metti alla prova le tue conoscenze.

.. mchoice:: question1_intro
     :multiple_answers:
     :correct: b,c
     :answer_a: Tutte le parole sono ugualmente probabili.
     :answer_b: La parola nella posizione (n) compare (1/n) volte più spesso di quella più frequente.
     :answer_c: La probabilità di un'osservazione è inversamente proporzionale al suo rango.
     :answer_d: Dovremmo leggere più libri.
     :feedback_a: Non proprio. Pensa quanto è popolare la parola "the" e quanto impopolare è la parola "Brexit".
     :feedback_b: Vero. Questa è l'affermazione più comune per la legge di Zipf.
     :feedback_c: Corretto. Questa sarebbe l'affermazione più generale per la legge di Zipf.
     :feedback_d: Anche se dovremmo assolutamente leggere molto, Zipf non l'ha mai detto.

     Cosa significa la legge di Zipf?


.. fillintheblank:: intro_fill_01

    Qual è il comando Python per loop?

    - :for|while: Esattamente!
      :.*: Non proprio... si prega di fare riferimento al testo sopra.


.. index:: comment

Commenti
=========

Il carattere ``#`` precede i commenti Python.
Tutti i caratteri che seguono ``#`` e si estendono fino alla fine della riga sono
ignorato [#comment]_.

.. code-block:: python

   >>> la prima riga del commento
   >>> print("Hello World!") # seconda riga del commento

La documentazione di funzioni o classi costituita da testo racchiuso tra virgolette triple o apostrofi può essere considerata una sorta di commento. Non è un commento *per sé*, ma potrebbe esserlo. È possibile leggere ulteriori informazioni sulla documentazione più avanti in questo manuale.

.. code-block:: python

   """
   questo testo è preceduto da
   e terminava con tre virgolette,
   è, quindi, un carattere ordinario letterale,
   per cui viene spesso utilizzato
   creazione della documentazione del programma
   """

   Un linguaggio di programmazione
   ========================

   Un programma per computer creato utilizzando un linguaggio di programmazione è il metodo più comune per fare in modo che un computer esegua il lavoro che abbiamo pianificato. Se questo lavoro non consiste nel rispondere alle e-mail ma piuttosto nello svolgere alcune attività ricorrenti che sono meno standard, allora scrivere il tuo programma sarà probabilmente il metodo più semplice. Può sembrare strano all'inizio dell'apprendimento della programmazione, ma speriamo che cambi idea entro la fine di questo corso. Tutti possiamo capire il linguaggio di programmazione come un metodo di comunicazione con un computer comprensibile per un essere umano. Possiamo paragonarlo a un linguaggio naturale che consente la comunicazione tra le persone. C'è una differenza, tuttavia. Nel caso dei linguaggi di programmazione, non c'è spazio per l'eufemismo o l'interpretazione. Dobbiamo esprimere tutti i messaggi in modo molto preciso. Il computer è una macchina e non può (ancora) elaborare l'effettiva espressione ambigua.



.. rubric:: Note a piè di pagina

.. [#comment] a volte può causare problemi tra lo standard di codifica del testo utilizzato nello script e quello dichiarato nell'intestazione dello script. È più sicuro utilizzare il sistema UTF-8.
