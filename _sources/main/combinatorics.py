def factorial(n):
     '''
     Function  calculates the factorial.

     IN:
       n (INT) - natural number

     OUT:
       INT: factorial of n! = 1 * 2 * 3 * ... * n
     '''
     f = 1
     for number in range(2, n + 1):
       f = f * number
     return f


def chance_of_k_wins_in_n_coin_tosses (k, n):
     '''
     A function that calculates the chance of k successes in n plain coin tossing attempts

     chance = n! / (k! * (n-k)!) * 0.5^n

     IN:
       k (INT) - number of successes
       n (INT) - number of attempts

     OUT:
       FLOAT: chance of k successes in n trials
     '''
     chance = factorial(n) / factorial(k) / factorial(n-k) * 0.5**n
     return chance
