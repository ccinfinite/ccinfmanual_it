.. _branching:

********************
Ramificazione
********************

.. index:: bool, boolean, True, False

Abbiamo già parlato del tipo di dati logici ``bool``. All'interno di questo tipo, possiamo distinguere solo due valori: ``True`` o ``False``. Molti linguaggi di programmazione prendono il numero ``0`` per falso e ``1`` (o qualsiasi altro numero in generale) per verità.
Esistono operatori logici standard definiti per questo tipo.

.. index:: casting, explicit casting

Il nome del tipo logico è anche il nome della funzione ``bool(obj)``, che permette di cambiare (cast) la variabile ``obj`` in un tipo logico.

.. activecode:: en_main_0601
   :caption: bool, boolean type

   print("bool(1):", bool(1))
   print("bool(-3):", bool(-3))
   print("bool(0):", bool(0))
   print("bool(None):", bool(None))
   print("bool([1, 2, 3]):", bool([1, 2, 3]))
   print("bool([]):", bool([]))
   print("bool('Andy has a cat'):", bool('Andy has a cat'))
   print("bool(''):", bool(''))


Sappiamo dalle lezioni precedenti che gli operatori logici di base sono ``e`` (logico e), ``o`` (logico o) e ``non`` (negazione logica). Troverai anche una panoramica degli operatori di confronto (``<``, ``==``, ecc.). Le operazioni con essi producono anche letterali del tipo ``bool``.
Questo tipo è la base per la costruzione di rami di programma.

**Branching** è un luogo in un programma per computer in cui decidiamo cosa dovrebbe fare il programma in seguito, in base a una struttura logica. Nel caso più semplice, il programma è diviso in due parti.

L'istruzione ``if``
====================

.. index:: if, branch

In Python, l'istruzione di ramificazione del programma è ``if``. Consideriamo un problema del genere.

   Se il valore della variabile ``x`` è negativo, cambialo in un numero positivo, altrimenti non fai nulla.

Possiamo *tradurre* un tale compito matematico in un linguaggio di programmazione.

   Se la variabile ``x`` è minore di 0, cambia il suo segno in quello opposto, altrimenti non fai nulla.

Come puoi vedere, abbiamo due possibilità: o il numero ``x`` è negativo o non lo è. Abbiamo a che fare con la ramificazione. Per scrivere un programma che svolga tale compito, dobbiamo usare l'istruzione ``if``.

.. code-block:: Python

    if x <0:
        x = -x

.. topic:: Esercizio

     Questo non è l'unico modo per risolvere il problema di cambiare i numeri negativi in positivi. In quale altro modo puoi cambiare i numeri da negativi a positivi? Prova a trovare altri due modi e scrivi un programma in Python.

..
  x *= -1
  x -= 2 * x
  x = abs(x)
  x = (x ** 2) ** 0.5

.. index:: indentation, code block

.. nota:: **Rientro**

     Per indicare che una data parte del codice appartiene a un dato ramo dell'istruzione ``if``, dobbiamo indentare in Python, cioè spostare una parte del codice di alcuni spazi a destra. È consuetudine utilizzare esattamente quattro spazi per distinguere un dato **blocco di codice**. Quando si impara a programmare, può sembrare ridondante e ingombrante. Fortunatamente, la maggior parte dei word processor lo fa automaticamente per noi. È sufficiente iniziare un determinato blocco di codice con **due punti** ``:``, affinché l'editor sposti automaticamente la riga di codice successiva di quattro spazi. Se lo desideri, puoi utilizzare un numero qualsiasi di spazi (e anche tabulazioni) per indentare un blocco di codice, ma i migliori programmatori Python ne usano quattro.


Lo schema di ramificazione completo di Python si presenta così:

.. code-block:: python

    if CONDITION:
        BLOCK_IF
    elif CONDITION_1:
        BLOCK_1
    elif CONDITION_2:
        BLOCK_2
        ...
    else:
        BLOCK_ELSE

Vediamo due nuove affermazioni: ``elif`` (che possiamo leggere come *else if*) e ``else`` (*altrimenti*). Il costrutto obbligatorio consiste nell'usare la parola chiave ``se`` e tutte le altre parole sono opzionali. Servono ramificazioni leggermente più complicate. Python verificherà le condizioni di cui sopra una per una. Se incontra una **condizione** che è vera, non verificherà più il resto di esse. Inoltre, non interpreterà nemmeno i blocchi rimanenti. L'interprete convaliderà solo la sintassi.

Diamo un'occhiata a un esempio. Verificheremo se la variabile ``number`` punta a un numero pari.

.. activecode:: en_main_0602
   :caption: even numbers

   number = 12
   if number % 2 == 0:
       e_or_ne = 'an even number'
   else:
       e_or_ne = 'not an even number'
   print(e_or_ne)

Ovviamente, nell'esempio sopra, vedremo il messaggio ``un numero pari``. Gioca con il codice sopra e controlla per quali numeri otteniamo numeri pari e per quali no. La parità di un numero significa che è divisibile per due senza resto, il che significa che il calcolo del resto della divisione utilizzando l'operatore ``%`` dovrebbe restituire zero. Stiamo testando questo fatto nel primo ramo ``if number% 2 == 0:``. Possiamo rispondere "sì" o "no" alla domanda di parità. Non c'è altra opzione. In tal caso, se il numero risulta essere non pari (la condizione discussa sarebbe uguale a ``False``), possiamo utilizzare l'opzione "in ogni altro caso" perché è l'unico altro caso. Pertanto, abbiamo usato il costrutto ``if-else``. Ci troviamo di fronte a una tale ramificazione binaria molte volte.

Per mostrare un ramo leggermente più complicato, scriveremo un programma che ci informerà di quanti giorni ha un determinato mese. Per distinguere tre possibili risposte (31, 30 e 28/29), dovremmo usare il costrutto ``if-elif-else``. Inoltre, possiamo proteggerci da una risposta sbagliata se l'utente del nostro programma fornisce un numero di mese al di fuori dell'intervallo $[1, 12]$. In questo caso, avremo bisogno di due istruzioni ``elif``.

.. activecode:: en_main_0603
   :caption: Numero di giorni nel mese.

   days_31 = [1, 3, 5, 7, 8, 10, 12]
   days_30 = [4, 6, 9, 11]
   month = int(input('Enter the number of the month (from 1 to 12): '))
   if month == 2:
       print('Month nr {} has 28 or 29 days.'.format(month))
   elif month in days_31:
       print('Month nr {} has 31 days.'.format(month))
   elif month in days_30:
       print('Month nr {} has 30 days.'.format(month))
   else:
       print('A year has 12 months.')


.. topic:: Esercizio

     Di solito, quando chiediamo un mese, intendiamo il suo nome, non l'ordine dell'anno. Prova a riscrivere il programma sopra in modo che chieda all'utente il nome del mese e, sulla base, restituisca informazioni sul numero di giorni. Probabilmente sarà meglio programmare la soluzione in qualche altro ambiente, non qui in *ActiveCode*. Per semplificare scarica :download:`questo script <it_main_ch06e03.py>`.
