.. _modules:

.. index:: module

**********
Moduli
**********

.. index:: decomposition, abstraction

Quando creiamo un programma per computer e risolviamo un problema con il suo aiuto, di solito costruiamo diverse funzioni per risolvere parti specifiche del problema. In :ref:`una lezione sulle funzioni <subroutines>` abbiamo scritto due funzioni: una calcolava il fattoriale e l'altra la probabilità di un certo numero di vincite in pochi lanci di monete. La prima (``factorial``) era una funzione ausiliaria che consentiva la riduzione del codice, ne migliorava la leggibilità e limitava la possibilità di commettere errori. In altre parole, ha portato ad una **scomposizione** del problema principale in sottoproblemi (o meglio le loro soluzioni). Inoltre, ha reso l'uso futuro della funzione fattoriale indipendente dalla sua implementazione. Potremmo migliorare questo codice in qualche modo (ad esempio, iniziare ``range`` con il numero 2 invece di 1). Ciò non influirebbe sulla soluzione del problema del lancio della moneta, che abbiamo costruito con la funzione ``factorial``. Tale indipendenza dell'implementazione dall'utilizzo è chiamata **astrazione**. La decomposizione e l'astrazione sono i due motivi principali per utilizzare le funzioni.

.. index:: import

Ci sono più problemi di combinatoria che richiedono il calcolo dei fattoriali. Tutto sommato, se dovessimo lavorare su problemi di combinatoria, sarebbe bello avere questa funzione a portata di mano e usarla senza dover definire ogni volta la funzione fattoriale. Un'opzione è usare tale funzione programmata nel modulo ``math``. Per usarlo, dobbiamo importarlo.

.. code-block:: python

  from math import factorial

D'ora in poi, possiamo usarlo proprio come abbiamo usato la funzione che abbiamo scritto. Quindi possiamo calcolare in quanti modi diversi possiamo formare le lettere "abc". Useremo la formula per la permutazione di un insieme di 3 elementi - la risposta è :math:`3!` (guarda qualsiasi libro di testo di matematica)

.. activecode:: en_main_1101
   :caption: Permutazioni e fattoriale

   from math import factorial
   word = 'abc'
   ans = factorial(len(word))
   print('{}! = {}'.format(len(word), ans))

Tecnicamente parlando, un modulo matematico è solo un file che contiene definizioni e istruzioni Python. Il nome del file è il nome del modulo aggiunto con l'estensione ``.py``.

.. index:: import, from

Uso dei moduli
=====================

Per utilizzare le funzioni nei moduli, dobbiamo importarle nello spazio dei nomi. Abbiamo due opzioni. Puoi trovare il primo sopra.

.. code-block:: python

  from module import object
  # per esempio
  from math import factorial

Possiamo ora fare riferimento a un ``oggetto`` (una funzione) come se fosse definito da noi stessi nel programma su cui stiamo lavorando, proprio come l'esempio sopra. Se vogliamo, possiamo cambiare il nome dell'oggetto importato in quello che ci piace la costruzione ``from module import object as nice_name``.

.. activecode:: en_main_1102
   :caption: Importazione di una funzione da un modulo

   from math import factorial as fact
   word = 'abc'
   ans = fact(len(word))
   print('{}! = {}'.format(len(word), ans))


Se abbiamo bisogno di più funzioni da un modulo, possiamo importarle come un elenco.

.. code-block:: python

  from module import object1, object2
  # per esempio
  from math import factorial, pow

Consentirà l'uso di entrambe le funzioni nel programma. Nell'esempio seguente, abbiamo importato le funzioni ``factorial`` come ``fact`` e la funzione ``pow`` con nome invariato.

.. activecode:: en_main_1103
   :caption: Import two functions from the module

   from math import factorial as fact, pow
   k, n = 5, 10
   print('The probability {} of wins in {} coin tosses equals '.format(k, n), end = '')
   print('{:.1f}%'.format(100 * fact(n) * pow(0.5, 10) / fact(k)**2))


Il secondo modo è importare l'intero modulo e fare riferimento alle funzioni (e altri oggetti) che contiene con un riferimento *punto*. Anche qui possiamo importare un modulo usando un nome che ci si addice di più.

.. code-block:: python

    import module
    import module as other_name

Calcoleremo ancora una volta in quanti modi possiamo disporre le lettere "abc", ma chiameremo la funzione tramite il modulo e il punto di riferimento.

.. activecode:: en_main_1104
   :caption: punto di riferimento a una funzione

   import math
   word = 'abc'
   ans = math.factorial(len(word))
   print('{}! = {}'.format(len(word), ans))


Il nostro modulo
=================

Se vogliamo costruire il nostro modulo contenente, ad esempio, varie funzioni utili in combinatoria, è sufficiente creare un file con un nome significativo e includere queste funzioni in questo file. Nel nostro caso chiameremo il file, e quindi il modulo ``combinatorics.py``. Aggiungeremo tutte le funzioni che abbiamo programmato nelle lezioni precedenti.

.. literalinclude:: combinatorics.py
    :linenos:
    :emphasize-lines: 1, 17
    :caption: Scarica :download:`modulo combinatoria <combinatorics.py>`.

Per utilizzare la libreria appena creata, è necessario importarla utilizzando uno dei metodi sopra descritti.

.. literalinclude:: it_main_ch11e01.py
    :linenos:
    :emphasize-lines: 1
    :caption: Scarica :download:`questo copione <it_main_ch11e01.py>`.
