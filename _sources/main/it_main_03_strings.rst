.. _strings:

*******
Stringa
*******

.. index:: stringa, stringa, str, indice

Creiamo una stringa di caratteri usando una coppia di apostrofi ``"..."`` o virgolette ``'...'`` tra i quali puoi inserire qualsiasi carattere trovi sulla tastiera, e anche quelli che vuoi non trovare su di esso.
In Python, un tipo che rappresenta una stringa è chiamato ``str``.

Possiamo guardare la stringa ``'Spam and Eggs'`` come se lo fosse
un insieme di caratteri, dove il primo carattere è ``S``, il secondo è ``p``, il terzo è ``a`` e così via. Quindi possiamo numerare i caratteri in una tale stringa. In Python, è consuetudine iniziare la numerazione da ``0``. In tal caso, il primo carattere nella stringa sarà rappresentato dal numero ``0`` e l'ultimo carattere dal numero ``string_length - 1``. Per la nostra stringa, la lettera ``S`` corrisponderà al numero ``0``, la lettera ``p`` al numero ``1``, la lettera ``e`` al numero `` 2``, ecc. Questi numeri (0, 1, 2 ...) sono chiamati **indici**.
Gli indici vengono utilizzati per puntare a un punto specifico in una stringa di caratteri. Quando si punta a un punto particolare, ad esempio quinto, ci si riferisce a un carattere con indice 5. Il quinto indice sarà, in questo caso, il sesto carattere della stringa (nel nostro caso, la lettera ``a``).
Tale riferimento si fa tra parentesi quadre:

.. code-block:: python

    string[index]

Sulla base della stringa ``'Spam and Eggs'``, faremo riferimento agli indici ``2`` e ``5``, cioè rispettivamente al terzo e sesto posto nella catena. Vediamo come funziona in *ActiveCode*

.. activecode:: en_main_0301
    :caption: Indicizzazione di sequenza

    text = 'Spam and Eggs'
    print('(a) text[2] =', text[2])
    print('(a) text[5] =', text[5])


.. index:: len, sequence length

L'ultimo indice nella variabile ``testo`` è il numero 12 ed è uguale a
``lunghezza_stringa - 1``. Per calcolare la lunghezza della sequenza, possiamo usare la funzione ``len('text')``, il che significa che l'ultimo indice disponibile della sequenza ``seq`` è dato dalla formula ``len(seq) - 1`` e con questa espressione possiamo fare riferimento all'ultimo elemento della sequenza.

.. code-block:: Python

     seq[len(seq) - 1]

Il penultimo elemento sarà ``seq[len (seq) - 2]`` ecc.
Stampare (e calcolare) la lunghezza della sequenza ogni volta non lo è
particolarmente vantaggioso, e Python ci permette di fare riferimento *dalla fine della sequenza* semplicemente usando indici negativi.

.. activecode:: en_main_0302
    :caption: indicizzazione della sequenza con indici negativi

    text = 'Spam and eggs'
    print('(last s) text[12] ->', text[12])
    print('(last s) text[len(text) - 1] ->', text[len(text) - 1])
    print('(last s) text[-1] ->', text[-1])
    print('(last g) text[-2] ->', text[-2])
    print('(S) text[-13] ->', text [-13])


indicizzazione della sequenza con indici negativi

=============== ========== ========== ========== ========== ==========
P               y          t          h          o          n
=============== ========== ========== ========== ========== ==========
0               1          2          3          4          5
len(s) - len(s) len(s) - 5 len(s) - 4 len(s) - 3 len(s) - 2 len(s) - 1
-len(s)         -5         -4         -3         -2         -1
=============== ========== ========== ========== ========== ==========

Operazioni sulle stringhe
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. index:: concatenation

Possiamo eseguire varie operazioni su variabili semplici come i numeri. Possiamo sommarli, sottrarli... ne abbiamo parlato :ref:`nel capitolo precedente <variables>`. Non è diverso con le catene. Possiamo sommare queste stringhe usando l'operatore ``+``.

.. code-block:: python

     >>> 'Spam' + 'eggs'
     'Spameggs'

Possiamo anche moltiplicarli per un numero (sebbene solo del tipo ``int``).

.. code-block:: python

     >>> 'Spam' * 3
     'SpamSpamSpam'

Provate voi stessi

.. activecode:: en_main_0303
     :caption: semplici operazioni sulle stringhe

     print('Spam' + 'eggs')
     print('Spam' * 3)

Tutto funziona allo stesso modo quando catturiamo per la prima volta la stringa in una variabile.

.. .. showeval:: showEval_concat_multi
..    :trace_mode: true
..
..    t1 = 'Spam'
..    t2 = 'eggs'
..    ~~~~
..    {{'Spam' + 'eggs'}}{{t1 + t2}}
..    {{'Spam' * 3}}{{t1 * 3}}


.. activecode:: en_main_0304
     :caption: semplici operazioni su variabili stringa

     text = 'Spam'
     other_text = 'eggs'
     print(text + other_text)
     print(text * 3)


Dai un'occhiata allo shortcode qui sotto.

.. code-block:: python
    :linenos:

    text = 'Buy an egg'
    result = (text[8] + text[4]) * 2


.. fillintheblank:: fitb-en_main_03ex1

   Quale valore assegnerai alla variabile 'risultato'?

   - :gaga: Esatto.
     :ga: Vedi la moltiplicazione per 2 alla fine.
     :e e e: In Python, l'indicizzazione inizia da 0.
     :x: Purtroppo no. Dai un'occhiata ai numeri tra parentesi quadre: a quali lettere si riferiscono questi numeri? Metti insieme quelle lettere e non dimenticare di moltiplicare per 2. Forse non hai incluso gli apostrofi (o le virgolette) nella tua risposta?


Possiamo confrontare le stringhe tra loro. Se vogliamo chiedere se due stringhe sono uguali (costituite dagli stessi caratteri), possiamo chiedere.

.. activecode:: en_main_0305
      :caption: confronto di stringhe

      text = 'Spam'
      same_text = 'Spam'
      other_text = 'eggs'
      print('Is Spam a Spam?', text == same_text)
      print('Is Spam not eggs?', text != other_text)

È un po' più complicato per altre operazioni di confronto. In questo caso, Python confronta le stringhe elemento per elemento. Per una coppia di caratteri, l'interprete confronta i valori dei caratteri dalla tabella dei caratteri data (ASCII, Unicode ...). Se i valori differiscono nella coppia di caratteri successiva, la stringa con il valore maggiore nell'array di caratteri viene trovata come quella maggiore. Puoi saperne di più sui confronti delle stringhe nella sezione sulla manipolazione delle stringhe.

.. _sequence_slicing:

Affettatura in sequenza
~~~~~~~~~~~~~~~~~~~~~~~~

Possiamo anche usare gli indici per fare riferimento a una parte più ampia di una sequenza. Possiamo usarli in *sequence slicing*. Un metodo generale per affettare la sequenza è il seguente:

.. code-block:: python

    seq[start:stop:step]

Come puoi vedere, generalmente dobbiamo dare tre numeri:

* ``start`` - l'indice da cui iniziamo lo slicing
* ``stop`` - indice a cui termina lo slicing, il valore corrispondente a questo numero viene omesso
* ``step`` - la dimensione dell'incremento

Diamo un'occhiata a un semplice esempio. Si noti che per tutti gli argomenti, possiamo sostituire sia numeri positivi che negativi e zero.

.. activecode:: en_main_0306
    :caption: affettatura in sequenza

    text = 'Spam and eggs'
    print('(egg) from 9 to 12 every 1:', text[9:12:1])
    print('(mne) from 3 to 12 every 3:', text[3:12:3])
    print('(Sa n gs) from start to finish every 2', text[0:len(text):2])
    print('(sgge dna) from the end to 4 with a negative step', text [-1:4:-1])

Un particolare tipo di passo è quello che è uguale a ``1``. Se ``step = 1`` significa che si intende ogni indice successivo nell'intervallo desiderato. È un valore predefinito dell'incremento e possiamo ometterlo.

.. code-block:: python

  >>> text[2:5] == text [2:5:1]
  True

Gli indici specifici sono ``0`` e ``-1`` (``len(seq) - 1``). Intendono
l'inizio e la fine della sequenza. Questi luoghi sono ovvi per l'interprete e possiamo saltarli.

.. activecode:: en_main_0307
    :caption: Indici 0 e -1

    text = 'Spam and eggs'
    print('from start to 5 every 1:', text[:5])
    print('from 8 to the end of every 2:', text[8::2])
    print('a copy of sequence - beginning to end every 1', text[:])
    print('reversed sequence', text[::-1])

Riferimenti simili possono essere fatti a qualsiasi tipo di sequenza.

Torniamo al compito precedente. Questa volta vogliamo usare la variabile ``text`` per assegnare ``'nana'`` alla variabile ``result``, ma questa volta usando la struttura di taglio delle stringhe sopra.

.. code-block:: python
    :linenos:

    text = 'Buy an egg'
    result = text[start:stop:-1] * 2

.. fillintheblank:: fitb-pl_main_03ex2

    Come apparirà il codice in modo che la variabile di risultato contenga la stringa ``'nana'``?

    result = tekst[|blank|:|blank|:-1] * 2

    - :5: Esatto.
      :6: Python numera le voci nella stringa da zero.
      :x: Purtroppo no.
    - :3: Esatto.
      :4: Non proprio. Ricorda che il secondo valore non è incluso nel ritaglio. Inoltre, Python numera le voci nella stringa da zero.
      :x: Purtroppo no.


.. index:: operator in, operator not in, in, not in

Operatori associativi
----------------------

Esistono speciali operatori di appartenenza per le sequenze. L'operatore `` in`` controlla se un elemento è presente nella sequenza. L'operatore gemello ``not in`` controlla se manca un pezzo.

.. activecode:: en_main_0308
     :caption: Operatori associativi: in, not in

     text = 'Spam and eggs'
     print('Is S in the text?', 'S' in text)
     print('Is z not in the text?', 'z' not in text)

Come puoi vedere, il risultato di queste operazioni è un valore logico, `` True`` o `` False``.

.. _strings_concatenation:

.. index:: concatenation, str function

Concatenazione di stringhe e funzione `` str``
-------------------------------------------------- -

In uno degli esempi precedenti, abbiamo usato l'operatore `` + '' per sommare due caratteri insieme. Possiamo concatenare stringhe di qualsiasi lunghezza e, di conseguenza, creare una nuova stringa più lunga.

.. activecode:: en_main_0309
     :caption: Concatenazione

     print('Spam' + 'and' + 'eggs')
     variable = 'Spam and eggs.' + ' ' + 'Eggs and spam.'
     print(variable)

Se vogliamo generare una sequenza di caratteri in questo modo allegando il numero (`` int`` o `` float``) ad una stringa, quindi semplicemente sommando tali quantità insieme, vedremo che otteniamo un errore.

.. index:: casting, function str

.. code-block:: python

    >>> number_of_cats = 2
    >>> 'Ala has ' + number_of_cats + ' cats.'
    TypeError: cannot concatenate 'str' and 'int' objects

In un certo senso, potremmo aspettarci questo perché l'operatore ``+`` denota qualcosa di diverso per numeri e stringhe. Per * dinamicamente * creare una stringa usando l'operatore ``+`` e la variabile numerica `` numero_di_gatti`` e ottenere ``'Ala ha 2 gatti.'``, dovremo in qualche modo convertire il numero ``2`` nel tipo ``str``.
È noto come **casting** dal tipo ``int`` al tipo ``str``. Per eseguirlo, utilizziamo la funzione con il nome del tipo richiesto. Qui sarà ``str`` poiché abbiamo bisogno di una rappresentazione in stringa del numero ``2``. Basta *avvolgere* la variabile che ci interessa con la funzione ``str(number_of_cats)``. Questa funzione restituirà la rappresentazione di una determinata variabile come tipo stringa.

.. activecode:: en_main_0310
    :caption: Concatenazione con colata

    number_of_cats = 2
    print ('Ala ma ' + str(number_of_cats) + ' cats.')

La maggior parte dei tipi incorporati ha questa rappresentazione e possiamo aggiungere qualsiasi altro letterale alle stringhe. Di seguito è riportato un esempio con un elenco, ma sentiti libero di provare anche diversi tipi!

.. activecode:: en_main_0311
    :caption: Una funzione che proietta str

    fib_list = [1, 1, 2, 3, 5, 8, 13, 21]
    fib_info = '8 words of the Fibonacci sequence are ' + str(fib_list)
    print(fib_info)

Il metodo ``format``
=======================

Le stringhe hanno un metodo integrato chiamato ``format``, che consente di creare le stringhe in modo piuttosto dinamico senza concatenazioni o una funzione di cast, che a volte può essere complicata. Il metodo `` format`` è un modo più semplice per farlo.

Se abbiamo tre variabili, ``a, b, c`` e vogliamo inserirle nella stringa usando la funzione `` format``, scriviamo:

.. code-block:: python

    '{}, {}, {}'.format(a, b, c)

Possiamo anche usare i numeri per specificare l'ordine degli argomenti. Consente all'ordine delle variabili nella stringa di essere diverso dall'ordine degli argomenti della funzione.

.. code-block:: python

    '{0}, {1}, {2}'.format(a, b, c)
    '{2}, {1}, {0}'.format(a, b, c)

Puoi anche usare le variabili più volte.

.. code-block:: python

    {0}{1}{0} '.format('abra', 'cad')

Possiamo anche usare i nomi degli argomenti.

.. code-block:: python

    'Coordinate: {lat}, {long}'.format(lat='37.24N ', long='- 115.81W')

È una buona idea usare la funzione 'format' per creare stringhe, poiché ti dà molto controllo sul risultato. Questa lezione tocca solo l'argomento delle stringhe e della loro formattazione. Raccomandiamo il `documento PyFormat <https://pyformat.info>` _ o leggere sulla manipolazione delle stringhe nel manuale gemello di questo progetto se vuoi saperne di più sulla formattazione delle stringhe.

.. activecode:: en_main_0312
    :caption: str.format()

    a, b, c = 'one', 'two', 'three'
    print('{}, {}, {}'.format(a, b, c))
    print('{0}, {1}, {2}'.format(a, b, c))
    print('{2}, {1}, {0}'.format(a, b, c))
    print('{0}{1}{0}'.format('abra', 'cad'))
    print('Coordinates: {latitude}, {longitude}'.format(latitude='37.24N ', longitude='-115.81W'))
