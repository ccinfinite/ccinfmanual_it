.. _io:

******************************
Ingresso e uscita
******************************

Le operazioni di input e output sono responsabili rispettivamente della lettura e della scrittura dei dati.
Le operazioni di base che ci interesseranno sono relative alla semplice comunicazione con l'utente. In definitiva, si intende visualizzare le informazioni sullo schermo e leggere i caratteri inseriti dalla tastiera. Le funzioni ``print`` e ``input`` sono usate rispettivamente per questo.

.. index:: print, simple print formatting

La funzione ``print``
~~~~~~~~~~~~~~~~~~~~~~

Per scrivere qualcosa sullo schermo, dobbiamo usare la funzione ``stampa``. Subito dopo il suo nome, dovremmo mettere l'oggetto che vuoi vedere tra parentesi. Può essere un valore dato esplicitamente o una variabile.

.. code-block:: python

    >>> print (13)
    13
    >>> z = 13
    >>> print (z)
    13

In questa semplice formattazione, visualizziamo solo il contenuto delle variabili. Possiamo anche mostrare una serie di variabili/valori. Basta separarli con la virgola, uno per uno.

.. activecode:: en_main_0401
    :caption: stampa - formattazione semplice

    name = 'Ala'
    age = 13
    print(name, "is", age, "years old")

.. topic:: Esercizio

     Usando l'esempio sopra, prova ad aggiungere qualche altra variabile e mostra che ``'Ala ha 13 anni, vive nella città di Katowice e il suo sport preferito è il basket.'`` con la funzione ``print``.

Argomenti opzionali per la funzione ``print``
-----------------------------------------------

.. index:: print argument sep

Il carattere di default che separa gli elementi della lista dei valori stampati dalla funzione ``stampa`` è uno spazio. Puoi vederlo nell'esempio sopra. Tuttavia, possiamo cambiare questo segno liberamente. L'argomento 'sep' viene utilizzato per questo. Per modificare lo spazio predefinito per un segno '+', digitare

.. code-block:: python

    >>> print('Ala', 'is', '13', 'years', 'old', sep='+')
    'Ala+is+13+years+old.'

Per sostituire ``+`` con ``---`` è sufficiente sostituire il separatore dato con quello nuovo desiderato.

.. showeval:: showEval_print_sep
   :trace_mode: false

   print('Ala', 'is', '13', 'years old.', sep='+')
   ~~~~
   print('Ala', 'is', '13', 'years old.', sep='{{+}}{{---}}')


Semplice, non è vero? Prova tu stesso. Sotto hai *ActiveCode*. Sostituisci ``+`` con un altro separatore, come: ``'@('_')@'``, vedrai come funziona.

.. activecode:: en_main_0402
   :caption: print - sep

   print('Ala', 'is', '13', 'years old.', sep='+')


.. index:: print argument end

Come con il separatore, possiamo determinare indipendentemente come ``print`` finirà la visualizzazione dell'elenco degli argomenti. Per questo, utilizziamo l'argomento ``end``. Per impostazione predefinita, ``print`` interrompe la riga e passa a una nuova, il che significa che ``end`` è impostato sul comando *break line*, che scriviamo ``end='\n'``. I caratteri ``\n`` sono caratteri speciali che indicano un'interruzione di riga. Ce ne sono di più. Se sei interessato, fai riferimento alla parte di questo manuale che si occupa della manipolazione delle stringhe.

Quando diamo quattro variabili in quattro chiamate alla funzione ``print``, le vedremo in quattro righe separate.

.. code-block:: python

   print('Ala')
   print('is')
   print('13 ')
   print('years old.')
   Ala
   is
   13
   years old.

D'altra parte, se impostiamo l'argomento ``end=''`` (o qualsiasi altra cosa, simile a ``sep``), vedremo che print non andrà su una nuova riga dopo aver visualizzato la stringa, ma invece di romperlo, metterà lì il carattere dato. Nota che l'ultima ``stampa`` nel codice qui sotto dovrebbe interrompere la linea.

.. activecode:: pl_main_0403
    :caption: print - end

    print('Ala', end=' ')
    print('is', end='... ')
    print('13', end="!!! ")
    print('years old.', end='\n')

.. index:: print argument flush, print argument file

Infine, menzioneremo altri due argomenti opzionali: ``file`` e ``flush``. Vengono utilizzati quando si scrive il contenuto delle variabili in un file. L'handle del file che vorremmo aggiungere viene passato alla variabile ``file``. Il secondo argomento, ``flush`` è una variabile booleana. Consente di forzare la stampa del contenuto immediatamente (quando lo impostiamo su ``True``) o di memorizzarlo nella cache (``False`` ). Non preoccupiamoci per ora, perché per ora non ne avremo bisogno.
Parleremo anche di file e handle in un'altra occasione.


L'istruzione ``input``
~~~~~~~~~~~~~~~~~~~~~~~~~

A volte vogliamo introdurre un po' di interazione nei nostri programmi, chiedere qualcosa all'utente e aspettarci una risposta (sì/no). Per passare un valore al nostro programma in modo dinamico, indipendentemente da un numero o da un testo, utilizziamo la funzione ``input(message)``. Questo ``messaggio`` è una stringa che l'utente vedrà prima di inserire un valore. Vediamo come funziona.

.. activecode:: en_main_0404
    :caption: input

    name = input("What's your name? ")
    age = input('How old are you? ')
    print(name, 'is', age, 'years old.')


I dati inseriti dall'utente non vengono interpretati e il tipo del valore memorizzato nella variabile è sempre una stringa (``str``).

A volte, tuttavia, i valori forniti dall'utente sono numerici e vorremmo catturarli in variabili come numeri per calcolare qualcosa da loro. Per abilitare ciò, è sufficiente castare esplicitamente ciò che la funzione ``input`` assume per il tipo di interesse. Per convertire i dati recuperati in un numero intero, dobbiamo usare la funzione int. Per i numeri in virgola mobile, useremo la funzione float.

.. activecode:: en_main_0405
    :caption: input, int and float

    cats = int(input('How many cats do you have?: '))
    print('You are at home', 4 * cats, 'furry legs!')

    height = float(input('How tall are you (in meters)? '))
    print('You have {} centimeters!'.format(height * 100))


.. topic:: Esercizio

     Utilizzando l'esercizio precedente e l'esempio precedente, scrivi un breve programma che chieda all'utente alcune informazioni aggiuntive. Oltre al nome e all'età, chiedi informazioni sul suo sport preferito e sulla città in cui vive. Infine, usa ``print`` per visualizzare queste informazioni il più chiaramente possibile. Puoi utilizzare gli esempi precedenti e il *ActiveCode* sopra per programmare la tua soluzione.
     
