import os

year = '2018'
picture_dir = 'Holidays in Bari'
working_dir = os.path.join(year, picture_dir)

for i, filename in enumerate(os.listdir(working_dir)):
    src = os.path.join(working_dir, filename)
    dst = os.path.join(working_dir,
            working_dir.replace('/', '_').replace(' ', '_') +
            "_" + str(i).zfill(3) + '.jpg')
    os.rename(src, dst)
