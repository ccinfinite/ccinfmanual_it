.. _concept_of_function:

*************************
Il concetto di funzione
*************************

Questa parte del tutorial sarà un'introduzione leggermente più formale alle funzioni. Forniremo una definizione completa e discuteremo i parametri formali, gli argomenti assegnati per nome e la dichiarazione di ritorno. Puoi prenderlo come uno sviluppo di ciò che abbiamo descritto nel capitolo precedente e guardare qui ogni volta che qualcosa relativo alle funzioni diventa poco chiaro.

.. index:: function

Come potresti aver letto prima, un pezzo di codice con nome a cui possiamo fare riferimento più volte sarà chiamato funzione. In Python, costruiamo funzioni con la parola chiave ``def``.

.. code-block:: Python

  def function_name (<argument_list>):
      BODY_FUNCTION
      <return object>

La caratteristica della funzione è il valore del risultato. In Python, questo valore è dichiarato dall'istruzione ``return`` all'interno del corpo della funzione.
L'utilizzo delle funzioni è intuitivo. Possiamo presumere che laddove sia necessario ripetere il codice o eseguire la stessa procedura, dovremmo trasformarlo in una funzione. Il programma diventa più leggibile e funzionale. L'idea di una funzione nella programmazione è analoga a una funzione in senso matematico. Tuttavia, a differenza delle funzioni matematiche, le funzioni di programmazione non necessitano di un argomento (parametro) per essere definite correttamente. Per esempio:

.. activecode:: en_main_1001
   :caption: Funzione senza parametri.

   def hey_function():
       return "Hey! How are you?"

   hey = hey_function()
   print(hey)

L'istruzione ``return``
========================

.. index:: return

Negli esempi precedenti, le funzioni interagiscono con noi tramite l'istruzione ``return``. Questa istruzione passa l'oggetto specificato e fa terminare il calcolo da parte della funzione. Se vogliamo passare più di una variabile, possiamo usare una tupla, una lista o un dizionario. Per chiamare una funzione, scrivi il suo nome seguito da una tupla contenente gli argomenti della funzione. L'ordine e il numero di parametri passati alla funzione devono rispettare la sua definizione:

.. activecode:: en_main_1002
   :caption: Funzione che calcola il valore del quadrato della somma.

   def squared_sum(x, y):
       return (x + y) ** 2

   print(squared_sum(2, 1))

Nota che puoi usare l'istruzione ``return`` più volte nel tuo codice funzione, ma la funzione smetterà di essere eseguita e di uscire quando viene raggiunta una di esse. Per esempio

.. activecode:: en_main_1003
   :caption: Una funzione che restituisce il maggiore dei due numeri dati.

   def maximum (x, y):
       """A function that returns the greater of two numbers."""
       if x > y:
           return x
       else:
           return y

   print(maximum(10, -10))

.. index:: docstring

L'esecuzione della funzione si interrompe dopo l'istruzione ``if`` o dopo l'istruzione ``else``. È buona norma descrivere a cosa serve la funzione sotto forma di stringa ``"""..."""``. Una descrizione, posta subito dopo la prima riga che definisce il nome della funzione, è denominata **docstring**. In questo corso, tendiamo a discutere i programmi in dettaglio. Per semplificare le cose, non rispetteremo sempre questa pratica nei seguenti esempi. Dovresti, tuttavia.

.. topic:: Esercizio

     Scrivi una funzione che controlli se il numero dato è pari o dispari.

Valori dei parametri predefiniti
=================================

.. index:: default parameters

Quando definiamo una funzione, possiamo assegnare valori di default ai parametri. I valori predefiniti verranno utilizzati quando non vengono forniti altri argomenti per questi parametri alla chiamata di funzione.

.. topic:: **Problema di fisica difficile**

     Hai lanciato la palla. Calcola quanto in alto salirà la pallina dopo \\(t\\) secondi se il valore della velocità iniziale era \\(v_0\\) m∕s. Trascura la forza di resistenza e tutto il resto tranne la gravità. Usa la formula

    .. math::

        h(t) = h_0 + v_0 t - \frac{1}{2} g t^2

Nella formula sopra, \\(g\\) è l'accelerazione gravitazionale, che per la Terra è approssimativamente \\(g = 9,80665 m/s^2\\). Consentiamo calcoli a bassa precisione e pretendiamo che \\(g\\) sia un valore costante (sebbene non lo sia ...). Poiché l'altezza e la velocità iniziali e il tempo in cui misuriamo l'altezza saranno diversi a seconda dell'esperimento, dovremmo lasciarli come variabili senza i valori predefiniti. Per l'accelerazione gravitazionale \\(g\\) possiamo supporre che, di norma, tale lancio avvenga a terra (\\(h_0=0\\). In questo caso possiamo dare i valori di default per l'accelerazione gravitazionale terrestre ``g`` e l'altezza iniziale \\(h_0\\) nella definizione della funzione ``altezza``.

.. activecode:: en_main_1004
   :caption: Funzione con valori di parametro predefiniti.

   def height(t, v0, h0=0, g=9.81):
       """Funzione altezza:
           t - tempo
           v0 - velocità iniziale
           h0 - altezza iniziale, il valore predefinito è zero
           g - accelerazione gravitazionale, di default per la Terra
        """
       return h0 + v0 * t - g * t ** 2 / 2

   print (height(0.3, 3))  # on Earth
   print (height(t=0.3, v0=3, g=1.622))  # on the Moon

.. topic:: Esercizio

     Scrivete una funzione che converta la distanza indicata in chilometri in miglia. Per impostazione predefinita, converti in miglia nautiche.


Un numero indefinito di parametri di funzione
==============================================

Python ti consente di costruire una funzione che non ha il numero specificato di parametri definiti. A tale scopo, far precedere il nome dell'elenco dei parametri di funzione dal simbolo ``*``. Possiamo chiamare tale funzione con un numero qualsiasi di argomenti. La funzione presentata di seguito riassume tutti i valori passati come argomenti.

.. activecode:: en_main_1005
   :caption: Una funzione con un numero indefinito di parametri.

   def addition(*arg):
       s = 0
       for a in arg:
           s += a
       return s

   print(addition(10))
   print(addition(10, 20, 30, 40))

Come funzionerà questo design insieme ad altri parametri? Vediamo.

.. activecode:: en_main_1006
   :caption: Un esempio di una funzione in cui il valore iniziale dei parametri è sconosciuto

   def addition(arg1, * arg):
       s = arg1
       for a in arg:
           s = s + a
       return s

   print(addition(100, 2))
   print(addition(100, 1, 2, 3, 4))

Funzioni di nidificazione
==========================

.. index:: nested functions

Possiamo includere definizioni di funzione all'interno del corpo di altre funzioni. Il programma seguente calcola il valore del coseno dell'angolo tra i vettori sul piano:

.. activecode:: en_main_1007
   :caption: Contenere una funzione al suo interno.

   def cosine(a, b):

       def length(x):
           return (x[0]**2 + x[1]**2)**0.5

       def product(x, y):
           return x[0]*y[0] + x[1]*y[1]

       m = length(a) * length(b)
       if m > 0:
           return product(a, b) / m

   print(cosine([1, 0], [0, 1]))


.. topic:: Esercizio

     Scrivete una funzione che per il dato ``a``, ``b`` e ``c`` trovi soluzioni al trinomio quadratico.

    .. math::

        a x^2 + bx + c = 0

    Il programma dovrebbe informare sul numero di soluzioni e restituirle.
