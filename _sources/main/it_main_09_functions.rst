.. _subroutines:

***************
Sottoprogrammi
***************

Di tanto in tanto, per prendere una decisione, decidiamo che il cieco destino deciderà per noi. In questi momenti, spesso `lanciamo una moneta <https://en.wikipedia.org/wiki/Coin_flipping>`_ e, a seconda che sia testa o croce, scegliamo una soluzione specifica. Ad esempio, le partite di calcio o di basket iniziano con il lancio di una moneta. Questo metodo garantisce che la selezione di una parte vincente sia completamente casuale e non dipenda da eventuali lanci precedenti. A volte risolviamo una disputa con un lancio del genere e speriamo che la moneta dimostri che abbiamo ragione. Forse vinciamo una, forse tre volte, ma ogni tiro è indipendente! Possiamo sempre calcolare la possibilità che (a) sempre o (b) la metà delle volte vinciamo. Usiamo la distribuzione binomiale per tali calcoli, che ci dirà la probabilità di :math:`k` successi in \\(n\\) prove.

.. math::

    \frac{n!}{k!(n-k)!} p^k (1-p)^{n-k}

Valore :math:`p=1/2` significa che abbiamo la stessa possibilità di ottenere testa o croce, :math:`p^k` denota l'esponenziale e :math:`n! = 1 \cdot 2 \cdot \dots n` sta per il fattoriale di \\(n\\). L'esponenziazione ha il suo operatore in Python e possiamo scrivere questa operazione come ``p ** k``. Factorial appare come una funzione nella libreria ``math``, ma è facile da calcolare usando la formula data e alcuni loop. Il codice seguente calcola il fattoriale di \\(10! = 3628800\\).

.. activecode:: pl_main_0901
    :caption: fattoriale

    n = 10
    factorial = 1
    for number in range(1, n + 1):
        factorial = factorial * number
    print(factorial)


Sappiamo già come calcolare fattoriale e potenza. Quindi possiamo calcolare la possibilità di vincere la scommessa cinque volte su 10 lanci. Torniamo alla formula, ma sostituiremo numeri specifici.

.. math::

    \frac{10!}{5!(10 - 5)!} 0.5^5 (1-0.5)^{10-5}

Dobbiamo calcolare il fattoriale di 10 e 5 e portare :math:`1/2` alla quinta potenza.

.. activecode:: en_main_0902
    :caption: 5 lanci di monete vinte in 10 tentativi
    :enabledownload:


    n = 10
    factorial10 = 1
    for number in range(1, n + 1):
        factorial10 = factorial10 * number

    n = 5
    factorial5 = 1
    for number in range(1, n + 1):
        factorial5 = factorial5 * number

    p5 = (factorial10 / (factorial5 * factorial5)) * (0.5 ** 5) * ((1 - 0.5) ** (10 - 5))
    print ('Chance to win five times in 10 tries is {: .1f}%'. format(p5 * 100))

Ti aspettavi il 50%? È facile cadere in questa trappola... Per fortuna non si tratta di un corso di combinatoria ma di un corso di programmazione. Torniamo al codice sopra per un momento. Le righe che calcolano il fattoriale di 10 (1-4) e 5 (6-9) sono copie dello stesso codice. L'unica differenza è il valore della variabile ``n`` ei nomi delle variabili di risultato (``factorial5`` e ``factorial10``). Probabilmente il modo più semplice per calcolare la probabilità di 5 vincite era se avessimo l'operatore ``!``, che calcola il fattoriale di un dato numero naturale. Non cambieremo la libreria standard di Python, ma in un certo senso possiamo programmare qualsiasi nuova funzionalità costruendo una **subroutine**. Possiamo dividere le subroutine in procedure e **funzioni**. In Python, ci occupiamo solo di quest'ultimo.

.. index:: function

Funzione
===============

Per funzione, intendiamo un pezzo di codice con nome a cui possiamo fare riferimento ripetutamente. Per costruire una funzione, utilizziamo la seguente sintassi.

.. code-block:: Python

  def function_name(<argument_list>):
      BODY_FUNCTION
      <return object>

Gli oggetti che vedi tra parentesi angolari (``argument_list`` e ``restituisci oggetto``) non sono obbligatori e possono essere omessi quando si costruisce una funzione. Di solito, tuttavia, la funzione trasformerà l'elenco degli argomenti (**dati di input**) in quell'``oggetto``, che in seguito restituirà (**dati di output**) con l'istruzione **return**. Per questo tutorial, concordiamo sul fatto che costruiremo funzioni contenenti la parola chiave **return**.

Proviamo a riscrivere il programma che calcola la possibilità di 5 vincite utilizzando la funzione fattoriale. I dati di input sono il numero ``n`` e i dati di output sono il fattoriale di questo numero.

.. activecode:: en_main_0903
    :caption: 5 lanci di monete vinte in 10 tentativi, questa volta con la funzione
    :enabledownload:

    def factorial(n):
        f = 1
        for number in range(1, n + 1):
            f = f * number
        return f

    factorial10 = factorial(10)
    factorial5 = factorial(5)
    p5 = (factorial10 / (factorial5 * factorial5)) * (0.5 ** 5) * ((1-0.5) ** (10-5))
    print ('Chance to win five times in 10 tosses is {: .1f}%'. format(p5 * 100))

Non è un operatore, ma è altrettanto semplice da usare: dobbiamo chiamare un nome di funzione (qui è ``factorial``), fornire l'elenco degli argomenti richiesti (ce n'è solo uno - ``n``) e catturare il valore restituito con qualche variabile.

.. code-block:: Python

  result = factorial(10)

Possiamo anche fare un ulteriore passo avanti e invece di calcolare la possibilità come sopra, possiamo anche scrivere una funzione che la calcoli. Possiamo anche nominarlo in modo che non lasci dubbi. Una volta programmato, possiamo usare la funzione ``fattoriale`` tutte le volte che vogliamo.

.. activecode:: en_main_0904
    :caption: 5 lanci di monete vinti in 10 tentativi, versione con due funzioni
    :enabledownload:

    def factorial (n):
        f = 1
        for number in range(1, n + 1):
            f = f * number
        return f

    def chance_of_k_wins_in_n_coin_tosses(k, n):
        chance = (factorial(n) / (factorial(k) * factorial(n-k))) * (0.5 ** k) * (0.5 ** (n-k))
        return chance

    print ('Chance to win five times in 10 tosses is {: .1f}%'. format (chance_of_k_wins_in_n_coin_tosses(5, 10) * 100))


.. topic:: Esercizio

   Prova a programmare una funzione che calcolerà la possibilità di \\(k\\) vincite in \\(n\\) tentativi per qualsiasi gioco, non solo *lancio di monete equo* dove testa e croce sono altrettanto frequenti, ma per giochi in cui non deve essere il caso (ad esempio, quando si lancia una moneta contraffatta). Dobbiamo scrivere una funzione che consideri i diversi valori della variabile :math:`p \in [0, 1]` nella prima formula di questa lezione, non solo 0,5 (come per la moneta fiera). In tal caso, la funzione deve avere un elenco di 3 argomenti ``k, n, p``. Puoi usare il *ActiveCode* sopra o programmare tale funzione nel tuo ambiente di sviluppo preferito.
   
