.. _for_loop:

*************
Ciclo ``for``
*************

.. index:: for

Questa istruzione ``for`` viene utilizzata per scorrere gli elementi di una sequenza.

.. code-block :: python

    for element in sequence:
        BLOCK_OF_INSTRUCTIONS

Tale ciclo avrà esattamente le rivoluzioni ``len(sequence)`` in modo che il blocco di istruzioni venga chiamato esattamente tante volte quanti sono gli elementi della sequenza. Diamo un'occhiata a un esempio.

.. activecode:: en_main_0801
    :caption: for loop example

    for el in 'Ala has a cat':
        print ('element: ' + el)

Possiamo anche dichiarare una variabile e quindi scorrere su di essa:

.. activecode:: en_main_0802
    :caption: another example of a sequence

    seq = (1, 25, 100, 10000)
    for el in seq:
        print('squared root of', el, "=", el ** 0.5)

Ovviamente, al posto della variabile ``seq`` possiamo mettere qualsiasi variabile con elementi: list, tuple, string o dictionary. Nel caso di quest'ultimo, itereremo sulle sue chiavi.

.. activecode:: en_main_0803
    :caption: dictionaries and for loop

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    for name in birthday:
        print(name, 'has birthday', birthday[name])


.. index:: range

Funzione ``range``
~~~~~~~~~~~~~~~~~~

``range`` è una pratica funzione che crea oggetti iterabili che possiamo usare direttamente in un ciclo ``for``. Crea una sequenza di numeri interi.

.. code-block:: python

    range(start, stop, step)

È abbastanza simile all'affettamento in sequenza. Questa funzione restituirà una sequenza di numeri, che inizia con il valore ``inizio`` e termina con ``stop - 1`` esattamente ogni ``passo``. Vediamo un esempio.

.. activecode:: en_main_0804

    seq = range(1, 11, 2)
    for el in seq:
        print(el)

Il codice sopra visualizzerà i numeri ``1, 3, 5, 7, 9``. I numeri ``start`` e ``step`` sono opzionali. Se diamo un solo numero ``A`` alla funzione ``range(A)`` otterremo tutti gli interi da ``0`` al numero ``A - 1``. Se diamo due numeri, il primo sarà interpretato come ``inizio`` e il secondo come ``stop``. Possiamo solo definire il passo specificando tutti e tre i numeri. Naturalmente, sono possibili quantità negative, incluso un valore negativo per un ``step``. Ci dà la possibilità di creare un elenco di numeri decrescenti. Dobbiamo ricordare che il valore iniziale dovrebbe essere maggiore del valore finale.

.. activecode:: en_main_0805
    :caption: per l'iterazione del ciclo attraverso la sequenza generata dalla funzione di intervallo

    for number in range(200, 100, -25):
        print(number)

.. topic:: Esercizio

     Rivisitiamo l'esercizio della lezione precedente. Usa la funzione ``range``, le istruzioni ``if`` e ``for`` per trovare la somma di tutti i numeri divisibili per 5 e 7 nell'intervallo da 2998 a 4444. Dovresti ottenere il valore 152110.

.. index:: iterator

La funzione 'range' crea un tipo speciale di sequenza chiamato **iteratore**. Non entreremo nei dettagli ora, ma poiché questo nome può essere trovato spesso in tutorial o libri sul linguaggio Python, vale la pena ricordare che se utilizziamo un tale iteratore in un ciclo, il risultato della sua operazione sarà lo stesso di la sequenza tipica (come le liste). Usando gli iteratori, risparmiamo memoria perché non creano un elenco completo nella memoria del computer, ma ad ogni iterazione calcolano il valore successivo, ricordando solo il valore attuale, il passaggio e il valore finale.

La funzione ``enumerate``
~~~~~~~~~~~~~~~~~~~~~~~~~~

A volte, avendo una sequenza, vogliamo scorrere quegli elementi che sono in qualche luogo di nostro interesse. Ad esempio, nella seconda metà di una lista o posizionati agli indici dispari. Possiamo usare con successo gli indici degli elementi, ma il ciclo ``for`` scorre sugli elementi, non sugli indici. Per non utilizzare soluzioni standard per questo, ovvero per introdurre una variabile aggiuntiva che svolgerà il ruolo di indice, come di seguito

.. code-block:: python

  idx = 0
  for element in sequence:
      if idx > len(sequence)/2:
          # do something with the elements
          # from the second half of the sequence

possiamo usare la funzione ``enumerate(sequence)`` che restituisce una tupla di ``(index, element)`` coppie. Per utilizzare entrambe le quantità contemporaneamente, possiamo scrivere

.. activecode:: en_main_0806
    :caption: example enumerate

    sequence = sorted([123, 12, 12, 1, 4, 1, 124, 13, 441])
    sum = 0
    for index, element in enumerate(sequence):
        if index > len (sequence)/2:
            sum += element
    print('Sum of items greater than median:', sum)
