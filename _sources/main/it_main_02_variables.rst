.. _variables:

*********
Variabili
*********

.. index:: letterale, operatore, primitive

Inizieremo discutendo i costrutti più semplici di cui sono costruiti praticamente tutti i linguaggi di programmazione.
Possiamo scomporre tali costruzioni *primitive* in **letterali**, che sono valori che il programmatore usa e **operatori** per manipolare questi letterali (valori).

Forse l'esempio più semplice è la somma di due numeri. Conosciamo tutti il risultato dell'addizione: math:`2 + 2`.
Per vedere il risultato dell'aggiunta di *ActiveCode* di seguito, dobbiamo racchiudere l'azione con la funzione ``print``.
Prova il codice qui sotto, quindi sostituisci i numeri e l'azione di addizione (``+``) con qualcos'altro che ricordi dalla tua lezione di matematica.

.. activecode:: it_main_0201
    :coach:
    :caption: Operazioni sui numeri

    print(2 + 2)

.. index:: variable, assignment operator

Possiamo anche ricordare il risultato dell'azione. A tale scopo, creiamo **variabili** nei linguaggi di programmazione. Una variabile è composta molto semplicemente: dobbiamo inserire il suo nome, quindi mettere il segno di uguale (**operatore di assegnazione**) e il valore che vogliamo assegnare a questa variabile, ad esempio:

.. code:: Python

   variable = 2

Nella finestra sottostante, puoi provare a creare variabili. In questo caso, assegniamo immediatamente il risultato della somma dei due numeri ``2 + 2`` alla variabile ``addition_result``, e la differenza ``2 - 2`` alla variabile ``subtraction_result``.


.. activecode:: en_main_0202
   :coach:
   :caption: Crea una variabile

   addition_result = 2 + 2
   print ('Addition result:', addition_result)
   subtraction_result = 2 - 2
   print ('Result of subtraction:', subtraction_result)


.. topic:: Esercizio

    Nella cella sopra, aggiungi nuove righe del programma, in cui calcolerai e stamperai (usando ``stampa``) il prodotto e il quoziente di due numeri qualsiasi. Puoi ancora usare ``2``, ma forse vuoi provare qualcosa di un po' più eccitante...

Come negli esercizi di matematica, i calcoli appropriati possono essere raggruppati tra parentesi. Lo facciamo per vari motivi: per evitare un errore o per specificare la sequenza di azioni. Se vogliamo sommare il valore raddoppiato dei due numeri 3 e 4, scriviamo ``2 * 3 + 2 * 4`` oppure


.. activecode:: it_main_0203
    :coach:
    :caption: Parentesi

     result = 2 * (3 + 4)
     print (result)


.. index:: data type

In generale, le parentesi vengono utilizzate per raggruppare le operazioni su qualsiasi variabile di qualsiasi *tipo* e non si limita alle operazioni con i numeri. Le parentesi consentono di controllare l'ordine in cui vengono eseguite le istruzioni. Nella maggior parte dei linguaggi di programmazione, possiamo utilizzare valori (letterali) con proprietà diverse. Il valore che forniamo e la rappresentazione visiva saranno definiti dal **tipo** dei dati.

.. topic:: Esercizio

    Durante lo shopping, hai trovato due paia di scarpe che vuoi acquistare. Quelle rosse costano 40 euro. Quelle azzurre erano 56 euro ma con il 25% di sconto. Per acquisti superiori a 85 euro avrai uno sconto aggiuntivo del 20%!

    Quale paio di scarpe costerà di più? Calcoliamo il prezzo scontato delle scarpe blu, quindi puoi spuntare la risposta appropriata di seguito.

    .. activecode:: it_main_0204
       :coach:
       :caption: Parentesi - Esercizio

       blue_price = 56
       discount = (100 - 25) / 100
       new_price = blue_price * discount
       print (new_price)

    .. clickablearea:: it_main_ex0201
      :question: Quale paio di scarpe costerà di più?
      :table:
      :correct: 1,2
      :incorrect: 1,1
      :feedback: Esegui sopra *ActiveCode* e controlla il risultato

      +--------+----------+
      | rosse  | azzurre  |
      +--------+----------+

    .. fillintheblank:: it_main_ex0202
       :casei:

       Quanto costeranno entrambe le coppie?

       - :82: Eccellente!
         :x: In modo errato. Prova a calcolare la somma nel *ActiveCode* sopra.

    .. clickablearea:: it_main_ex0203
          :question: Se decidi di acquistare entrambe le paia, potrai ottenere uno sconto aggiuntivo?
          :table:
          :correct: 1,2
          :incorrect: 1,1
          :feedback: Torna a *ActiveCode* e somma entrambi i numeri

          +------+------+
          | sì   | no   |
          +------+------+


Tipi di dati di base
======================

.. index:: int type, float type

Tipi numerici
~~~~~~~~~~~~~

Abbiamo visto diversi tipi di dati negli esempi precedenti. Letterali come ``2`` o ``2.0`` stanno per il numero 2 (un valore letterale numerico con il valore 2) ma di due tipi diversi. Il letterale ``2`` ha il valore 2 e gli viene assegnato il tipo intero (``int``). Quest'ultimo, ``2.0``, ha anche il valore 2, ma è di tipo a virgola mobile (``float``). Possiamo trattarli entrambi semplicemente come il numero 2, ma il modo in cui sono archiviati nella memoria del computer sarà diverso. Puoi leggere di più sui numeri nell'altra parte di questo progetto.

.. index:: bool type, True, False

Tipo booleano
~~~~~~~~~~~~~~~

Particolari tipi di dati differiscono nei valori impostati consentiti e nelle operazioni consentite che possiamo eseguire su di essi.
Uno dei tipi di dati più comunemente usati è il tipo logico (``bool``).
Può assumere i valori ``True`` per verità logica o ``False`` per falso logico. Ad esempio, sappiamo che il numero 2 è maggiore del numero 1. Per questo, scrivendo: math:`2> 1`, sappiamo che l'espressione è vera. Possiamo programmare una tale operazione.

.. code:: Python

    >>> 2 > 1
    True

Inoltre, possiamo assegnare il risultato di tale confronto (``True``) a una variabile.

.. activecode:: it_main_0205
    :coach:
    :caption: Tipo booleano

    print(2 > 1)
    truth = 2 > 1
    print(truth)

Se torniamo ora al problema dell'acquisto di scarpe, invece di giudicare quale paio costa di più guardando i conti, possiamo lasciare le conclusioni all'interprete.

.. activecode:: it_main_0206
    :coach:
    :caption: Problemi di scarpe

    red_shoes = 40
    blue_shoes = 56 * 0.75
    print (red_shoes > blue_shoes)


.. index:: comparison operator

Gli operatori di base sono associati al tipo logico, con il quale possiamo confrontare variabili e ottenere true o false come risultato di tale confronto. Abbiamo l'operatore ``>`` (maggiore di), che già conosciamo, ma anche l'operatore ``<`` (minore di). Possiamo anche verificare se una variabile è "maggiore o uguale a" (``>=``) e "minore o uguale a" (``<=``) un'altra variabile. Controlliamo l'uguaglianza usando il doppio segno di uguale ``==``, che è probabilmente la fonte di uno degli errori più comuni commessi dalle matricole. Testiamo le disuguaglianze usando ``! =``. Nel seguente *ActiveCode*, puoi provare gli operatori di confronto secondo le tue idee.

.. activecode:: it_main_0207
    :coach:
    :caption: Confronta le operazioni

    print(100 > 10)

Possiamo eseguire le operazioni di confronto su tutti gli oggetti. Hanno tutti la stessa priorità e l'interprete li analizzerà da sinistra a destra.
In Python, possiamo anche confrontare se due oggetti sono uguali (maggiori informazioni sugli oggetti nel capitolo su :ref:`programmazione orientata agli oggetti <oop>`). Gli operatori ``is`` e ``is not`` ci dicono proprio questo. Tuttavia, non saranno molto utili in questo corso.

.. index:: type NoneType, None

Digita 'Nessuno'
~~~~~~~~~~~~~~~~

C'è anche un particolare tipo di dati in Python chiamato ``NoneType``. All'interno di questo tipo, troviamo un solo valore, ``Nessuno``. Viene utilizzato per esprimere la mancanza di un valore significativo, l'inesistenza di dati o la memorizzazione di un contenuto *vuoto*. È anche il valore restituito dalla funzione quando omettiamo la parola chiave ``return`` o quando non diamo alcun valore dopo questa parola. Puoi saperne di più sulle funzioni nei capitoli :ref:`later <subroutines>` di questo manuale.

.. index:: type function

Tipo ``funzione``
~~~~~~~~~~~~~~~~~

Per verificare che tipo è un dato oggetto, puoi usare la funzione ``type(obj)``.

.. code-block:: Python

     >>> type(3)
     <type 'int'>
     >>> type(7.5)
     <type 'float'>
     >>> type('a')
     <type 'str'>
